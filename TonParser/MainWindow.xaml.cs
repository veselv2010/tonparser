﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using System.Diagnostics;
using TonParser._Math;

namespace TonParser
{
    public partial class MainWindow : Window
    {
        public int Age,
                   Growth,
                   Weight,
                   L1,
                   L2,
                   L5;
        //////////////////////////////////
        private byte lowerPressureControl = 103, //вынести на интерфейс...?
                     topPressureControl = 161;
        private double frequency { get; set; }  //частота влияет только на пульс (fss)
        private ushort[] pulseData { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            TextBox_TextChanged(TextBoxAge, null); //костыль для обновления значений при запуске
            TextBox_TextChanged(TextBoxWeight, null);
            TextBox_TextChanged(TextBoxGrowth, null);
            TextBox_TextChanged(TextBoxL1, null);
            TextBox_TextChanged(TextBoxL2, null);
            TextBox_TextChanged(TextBoxL5, null);
        }

        private void CheckBoxIsSimulation_Checked(object sender, RoutedEventArgs e)
        {
            Statics.isTimeSim = CheckBoxIsSimulation.IsChecked.Value;
            ButtonCalculate.IsEnabled = !ButtonCalculate.IsEnabled;
        }

        private void ButtonCalculate_Click(object sender, RoutedEventArgs e)
        {
            ushort[] tempPulseArray = Statics.GeneratedPulseData.ToArray();
            _new(null, tempPulseArray);
        }

        private void ButtonSelectFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == true)
                _new(fileDialog.FileName);
        }

        private void Recalculate(object sender, RoutedEventArgs e)
        {
            try
            {
                _new(TextBoxFilePath.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Файл не найден", "TonParser");
                return;
            }
        }
        //числа в текстбоксах при их изменении автоматически заносятся в одноименные поля этого класса
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.IsInitialized)
            {
                TextBox senderBox = sender as TextBox;
                int.TryParse(senderBox.Text, out int value);

                if (senderBox?.Text == "" || value == 0)
                    return;

                string senderName = senderBox.Name.Replace("TextBox", "");

                typeof(MainWindow).GetField(senderName).SetValue(this, value);

                if (this.IsLoaded || TextBoxFilePath.Text != "") 
                    Recalculate(null, null);                
            }
        }

        private void ToolTipL_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory + "/Resources/Casp.jpg");
        }

        private void _new(string filepath, ushort[] pulseData = null)
        {
            PulseWaveTags mathReper;
            if (pulseData == null)
            {
                var fileWorker = new FileWorker(filepath);
                this.TextBoxFilePath.Text = filepath;
                this.pulseData = fileWorker.PulseData;
                Statics.PulseData = fileWorker.PulseData;
                this.frequency = fileWorker.Frequency;
                mathReper = new PulseWaveTags(this.pulseData, frequency);
            }
            else
                mathReper = new PulseWaveTags(pulseData, frequency);

            //перенести все в один класс
            var globalCalc = new GlobalCalc(mathReper.PulseWaveField);
            globalCalc.MakeCalc(this.CheckBoxSex.IsChecked.Value, this.Age, this.Growth, this.Weight);

            this.LabelFss.Content = "Pulse: " + Statics.fss.ToString();
            this.LabelSosudIndex.Content = "Sosud index: " + mathReper.GetSosud(Statics.a2a1).ToString();
            this.LabelStress.Content = "Stress index: " + mathReper.GetStress().ToString();

            mathReper.Repers = globalCalc.Repers; //what did he mean by this

            byte topPressureResult = topPressureControl,
                 lowerPressureResult = lowerPressureControl; //контроль тонометром

            mathReper.DoTagsPulseWave(); //обновление разметки на волне с новыми реперами (?)
            mathReper.CalculatePulseWave();

            double polynomTop = mathReper.PolynomTop(Statics.a2a1, Statics.fss),
                   polynomLower = mathReper.PolynomLower(Statics.a2a1, Statics.fss);
            Statics._patientK1 = (float)(230f * polynomTop / topPressureResult); //давление пациента с другого прибора (?) 1.154f;
            Statics._patientK2 = (float)(230f * polynomLower / lowerPressureResult); //1.9437f;

            //отрефакторить этот метод
            topPressureResult = mathReper.CalculateADUpper(Statics.a2a1, Statics.fss, Statics._patientK1);
            lowerPressureResult = mathReper.CalculateADLower(Statics.a2a1, Statics.fss, Statics._patientK2);

            this.LabelTrueUpperLower.Content = "Контроль тонометром (?): " + 
                topPressureResult.ToString() + " / " + lowerPressureResult.ToString();
            this.LabelPressure.Content = $"AD: {globalCalc.ResultTopPressure.ToString()}/{globalCalc.ResultLowerPressure.ToString()}";
            CalculateCasp(mathReper, topPressureResult, lowerPressureResult);
        }
        //сделать биндинги на поля класса чтобы разгрузить код
        private void CalculateCasp(PulseWaveTags myPulseWave, int topPressureResult, int lowerPressureResult)
        {
            float timeA1A4 = myPulseWave.GetRepersSeconds(1, 4);
            this.LabelTimeA1A4.Content = "Time A1A4: " + timeA1A4.ToString();
            var _calc = new CaspCalc(myPulseWave.PulseWaveField, myPulseWave.Repers);
            _calc.MakeCalc(this.L1, this.L2, this.L5, timeA1A4);

            this.LabelSRPVp.Content = "SRPVp: " + CaspCalc.PrintStr(_calc.ResultSrpvp);

            myPulseWave.SetStartDelay(_calc.ResultDelta);

            var _myPulseWaveResult = PulseWave.DeepClone(myPulseWave); //по какой-то причине этот метод дает другой результат в переменной maxAllPulse
            _myPulseWaveResult.PulseWaveField = _calc.ResultPulseWave;
            _myPulseWaveResult.SetStartDelay(0);
            _myPulseWaveResult.Repers = _calc.ResultRepers;
            int maxAllPulse = Math.Max(myPulseWave.PulseWaveField.Max(), _myPulseWaveResult.PulseWaveField.Max());
            _myPulseWaveResult.CalcScalePress(topPressureResult, lowerPressureResult, maxAllPulse);
            var pressure = _myPulseWaveResult.ConvertToPress();

            // коэффициент - костыль для испытаний (комментарий с TonClock)
            double coef = 1.1d;
            if (topPressureResult > 120)
                coef = 1.15d;
            LabelCasp.Content = "CASP: " + CaspCalc.PrintStrShort(pressure.Max() * coef);
            //////////////////////////////////////////
            if (_myPulseWaveResult == null || _myPulseWaveResult.Repers == null) return;

            int p1 = _myPulseWaveResult.GetReper(1),
                p2 = _myPulseWaveResult.GetReper(2);

            if (_myPulseWaveResult.PulseWaveField == null || _myPulseWaveResult.PulseWaveField.Length <= p1 || _myPulseWaveResult.PulseWaveField.Length <= p2)
                return;

            double ia1 = (double)_myPulseWaveResult.PulseWaveField[p2] / _myPulseWaveResult.PulseWaveField[p1];
            double ia2 = (double)(_myPulseWaveResult.PulseWaveField[p2] - _myPulseWaveResult.PulseWaveField[p1]) /
                         _myPulseWaveResult.PulseWaveField[p2];

            LabelIA1.Content = "ИА1: " + CaspCalc.PrintStr(ia1);
            LabelIA2.Content = "ИА2: " + CaspCalc.PrintStr(ia2);
        }
    }
}
