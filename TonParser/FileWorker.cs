﻿using System;
using System.IO;

namespace TonParser
{
    internal class FileWorker
    {
        private int startDelay = 0;
        private string path { get; }
        private string fileName { get; }
        public ushort[] PulseData { get; set; } //волна
        public double Frequency{ get; }
        public int StartDelay
        {
            get
            {
                return startDelay;
            }
            set
            {
                startDelay = (int)Math.Round(value * PulseData.Length / 20f);
            }
        }
        public FileWorker(string path)
        {
            this.path = path;
            //открытие двоичного файла по пути расположения
            try
            {
                using (FileStream fileStream = new FileStream(this.path, FileMode.Open, FileAccess.Read))
                using (BinaryReader reader = new BinaryReader(fileStream))
                {
                    PulseData = new ushort[fileStream.Length / 2];

                    for (int i = 0; i < PulseData.Length; i++)
                        PulseData[i] = reader.ReadUInt16();

                    this.fileName = fileStream.Name;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return;
            }
            Frequency = this.PulseData.Length / 20d;
        }
    }
}
