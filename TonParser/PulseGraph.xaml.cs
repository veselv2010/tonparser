﻿using System;
using System.Windows.Controls;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace TonParser
{
    public partial class PulseGraph : UserControl
    {
        public PulseGraph()
        {
            InitializeComponent();
        }
    }
    public enum SimulationType
    {
        Waves,
        TimeSimulation,
        PulseGraph
    }

    public class MainViewModel : INotifyPropertyChanged, IDisposable
    {
        private int ushortIndex = 0;
        public int frequency { get; set; }
        public ushort[] pulseData ;
        private int UpdateInterval = 500;
        private bool disposed;
        private readonly Timer timer;
        private readonly Stopwatch watch = new Stopwatch();
        private SimulationType simulationType;

        public MainViewModel()
        {
            this.timer = new Timer(OnTimerElapsed);

            if (Statics.PulseData == null)
            {
                this.SimulationType = SimulationType.Waves;
            }
            else if (Statics.isTimeSim == true)
            {
                this.SimulationType = SimulationType.TimeSimulation;
            }
            else
            {
                this.SimulationType = SimulationType.PulseGraph;
                this.pulseData = Statics.PulseData;
            }
        }
    

        public SimulationType SimulationType
        {
            get
            {
                return this.simulationType;
            }

            set
            {
                this.simulationType = value;
                this.RaisePropertyChanged("SimulationType");
                this.SetupModel();
            }
        }

        private void SetupModel()
        {
            this.timer.Change(Timeout.Infinite, Timeout.Infinite);

            PlotModel = new PlotModel();
            PlotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Minimum = -2, Maximum = 2});
            PlotModel.Series.Add(new LineSeries { LineStyle = LineStyle.Solid });

            this.watch.Start();

            this.RaisePropertyChanged("PlotModel");

            this.timer.Change(1000, UpdateInterval);
        }

        public int TotalNumberOfPoints { get; private set; }
        public PlotModel PlotModel { get; private set; }

        private void OnTimerElapsed(object state)
        {
            lock (this.PlotModel.SyncRoot)
            {
                this.Update();
            }

            this.PlotModel.InvalidatePlot(true);
        }
        private int reuseCounter = 1;
        private int fullReuseCounter = 1;
        ushort[] temppulsedata;
        private void Update()
        {
            int n = 0;

            var s = (LineSeries)PlotModel.Series[0];

            this.pulseData = Statics.PulseData;

            if(this.pulseData != this.temppulsedata && !Statics.isTimeSim)
            {
                s.Points.Clear();
                PlotModel.Axes.Clear();
                PlotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Minimum = 0, MajorStep = 222, Maximum = 1400 });
                this.temppulsedata = this.pulseData;

                uint allH = (ushort)(this.PlotModel.Height - 10);
                uint min = pulseData.Min(), max = pulseData.Max();
                int curX = 0, iIndex = 0;
                while (iIndex < pulseData.Length)
                {
                    uint curH = (uint)(pulseData[iIndex] - min);
                    s.Points.Add(new DataPoint(curX, (int)(curH * allH / (max - min))));
                    curX++;
                    iIndex++;
                }
            }
            else
            {
                if (this.pulseData != this.temppulsedata)
                {
                    s.Points.Clear();
                    PlotModel.Axes.Clear();
                    PlotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Minimum = 0, MajorStep = 222, Maximum = 1400 });
                    this.temppulsedata = this.pulseData;
                    this.ushortIndex = 0;
                    this.reuseCounter = 1;
                    this.fullReuseCounter = 1;
                    Statics.GeneratedPulseData.Clear();
                }
                if (this.pulseData != null && Statics.isTimeSim)
                {
                    uint _min = pulseData.Min(), _max = pulseData.Max();
                    DrawPointsFromPartData(reuseCounter, _min, _max, s);
                    reuseCounter++;
                    if (220 * reuseCounter > pulseData.Length)
                    {
                        fullReuseCounter = 220 * reuseCounter / pulseData.Length;
                    }
                }
            }

            n += s.Points.Count;          

            if (this.TotalNumberOfPoints != n)
            {
                this.TotalNumberOfPoints = n;
                this.RaisePropertyChanged("TotalNumberOfPoints");
            }
        }
        private void DrawPointsFromPartData(int reuseCounter, uint min, uint max, LineSeries s)
        {
            uint allH = (ushort)(this.PlotModel.Height - 10);
            int tempCounter = ushortIndex;
            while (tempCounter < reuseCounter * 220)
            {
                uint curH;
                if (ushortIndex < pulseData.Length - 1)
                {
                    Statics.GeneratedPulseData.Add(pulseData[ushortIndex]);
                    curH = (uint)(pulseData[ushortIndex] - min);
                    s.Points.Add(new DataPoint(tempCounter, (int)(curH * allH / (max - min))));
                }
                else
                {
                    int temp = Math.Abs(pulseData.Length - ushortIndex / fullReuseCounter);
                    Statics.GeneratedPulseData.Add(pulseData[temp]);
                    curH = (uint)(pulseData[temp] - min);
                    s.Points.Add(new DataPoint(tempCounter, (int)(curH * allH / (max - min))));
                }
                tempCounter++;
                ushortIndex++;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.timer.Dispose();
                }
            }
            this.disposed = true;
        }
    }
}
