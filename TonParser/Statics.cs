﻿using System;
using System.Collections.Generic;

namespace TonParser
{
    public static class Statics
    {
        public static ushort[] PulseData;
        public static List<ushort> GeneratedPulseData = new List<ushort>();
        public static bool isTimeSim = false;
        public static float a2a1; //а2 / а1
        public static int fss; //пульс

        // Флаг, что масштаб - по ширине окна
        public static bool FlagScaleWidth = true;

        // Степени полинома
        public const float J = 0.15f;
        public const float N = 0.6f;
        public const float H = 1f;
        public const float I = 0.8f;
        public const float R = 0.1f;

        //константы(?) с бд TonClock
        public const float _patientFss = 80,
                           _patientA2A1 = 0.6f,
                           _patientS1 = 0.42f,
                           _patientS2 = 0.75f,
                           _patientK = 2,
                           _patientA = 0.7f,
                           _patientB = 2,
                           _patientC = 0.1f,
                           _patientD = 1;
        public static float _patientK1 = 1.917f,
                            _patientK2 = 2.875f;
        public const int   _patientFssOpor = 80;
    }
}
