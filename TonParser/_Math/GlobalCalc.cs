﻿//
//   Класс GlobalCalc для расчета давления без калибровки.
// (полином "глобал")
//

using System;
using System.Globalization;
using System.Linq;

namespace TonParser._Math
{
    class GlobalCalc
    {
        private ushort[] PulseWaveField;    // средняя волна

        private int[] _repers,          // реперы
                      derivative;      // производная

        public int[] Repers
        {
            set { _repers = value; }
            get { return _repers; }
        }

        public GlobalCalc(ushort[] PulseWaveField)
        {
            this.PulseWaveField = PulseWaveField;
            AutoRepers();
        }

        // авто-расстановка реперов
        public void AutoRepers()
        {
            if (PulseWaveField == null) return;
            derivative = new int[PulseWaveField.Length];
            for (int i = 0; i < PulseWaveField.Length - 1; i++)
                derivative[i + 1] = PulseWaveField[i + 1] - PulseWaveField[i];
            derivative[0] = derivative[1];
            _repers = new int[5];
            ushort max = PulseWaveField.Max();
            for (int i = 0; i < PulseWaveField.Length; i++)
            {
                if (PulseWaveField[i] == max)
                    _repers[0] = i;
            }
            _repers[1] = Math.Min(_repers[0] + 22, PulseWaveField.Length - 1);

            // ищем максимальную производную на 2/3 оставшегося участка
            int maxDer = _repers[1] + 1;
            for (int i = _repers[1] + 2; i < _repers[1] + (PulseWaveField.Length - _repers[1]) / 3 * 2; i++)
            {
                if (derivative[i] > derivative[maxDer])
                    maxDer = i;
            }
            //A3 - минимальное значение от этой максимальной производной до А2
            _repers[2] = Math.Min(_repers[1] + 1, PulseWaveField.Length - 1);
            float minDerA3 = (PulseWaveField[_repers[2] - 1] + PulseWaveField[_repers[2]] + PulseWaveField[_repers[2] + 1]) / 3f;
            for (int i = _repers[1] + 2; i < maxDer; i++)
            {
                float curDerA3 = (PulseWaveField[i - 1] + PulseWaveField[i] + PulseWaveField[i + 1]) / 3f;
                if (curDerA3 < minDerA3)
                {
                    _repers[2] = i;
                    minDerA3 = curDerA3;
                }
            }
            //A4 - максимальное значение от этой максимальной производной до конца
            _repers[3] = Math.Min(maxDer, PulseWaveField.Length - 1);
            float maxDerA3 = (PulseWaveField[_repers[3] - 1] + PulseWaveField[_repers[3]] + PulseWaveField[_repers[3] + 1]) / 3f;
            for (int i = maxDer + 1; i < PulseWaveField.Length - 1; i++)
            {
                float curDerA4 = (PulseWaveField[i - 1] + PulseWaveField[i] + PulseWaveField[i + 1]) / 3f;
                if (curDerA4 > maxDerA3)
                {
                    _repers[3] = i;
                    maxDerA3 = curDerA4;
                }
            }

            //здесь был закомментированный код
            /////////////////////////////////////////////////
            int[] repers = new int[(PulseWaveField.Length - _repers[1]) / 2];
            for (int i = 0; i < repers.Length; i++)
                repers[i] = _repers[1] + 1 + i;
            for (int i = 0; i < repers.Length; i++)
                for (int j = i + 1; j < repers.Length; j++)
                    if (Math.Abs(derivative[repers[j]]) < Math.Abs(derivative[repers[i]]))
                    {
                        int temp = repers[i];
                        repers[i] = repers[j];
                        repers[j] = temp;
                    }
            repers = (int[])repers.Take(20).ToArray();
            _repers[2] = 0;
            _repers[3] = 0;
            int count2 = 0, count3 = 0, srednee = 0;
            for (int i = 0; i < 20 && i < repers.Length; i++)
                srednee += repers[i];
            if (repers.Length > 0)
                srednee /= Math.Min(20, repers.Length);
            for (int i = 0; i < 20 && i < repers.Length; i++)
            {
                if (repers[i] < srednee)
                {
                    _repers[2] += repers[i];
                    count2++;
                }
                else
                {
                    _repers[3] += repers[i];
                    count3++;
                }
            }
            if (count2 != 0)
                _repers[2] /= count2;
            if (count3 != 0)
                _repers[3] /= count3;

            for (int i = _repers[1] + 1; i < _repers[3]; i++)
                if (PulseWaveField[i] < PulseWaveField[_repers[2]])
                    _repers[2] = i;
            for (int i = _repers[2] + 1; i < PulseWaveField.Length; i++)
                if (PulseWaveField[i] > PulseWaveField[_repers[3]])
                    _repers[3] = i;
            /////////////////////////////////////////////////////////////

            if (_repers[3] - _repers[2] < 2)
            {
                _repers[2] = _repers[1] + 11;
                _repers[3] = _repers[2] + 11;
            }

            _repers[4] = Math.Max(_repers[3] + 2, derivative.Length / 4 * 3 + 1);
            float minDer = float.MaxValue;
            for (int i = _repers[4]; i < derivative.Length - 2 - 10; i++)
            {
                float sredDerivative = 0;
                for (int j = -1; j <= 1; j++)
                    sredDerivative += Math.Abs(derivative[i + j]);
                sredDerivative /= 3;
                if (sredDerivative <= minDer)
                {
                    _repers[4] = i;
                    minDer = sredDerivative;
                }
            }
            if ((PulseWaveField.Length - 1) - _repers[4] < 5)
                _repers[4] = (PulseWaveField.Length - 1) - 11;
        }

        public void AutoRepers_OLD2()
        {
            if (PulseWaveField == null) return;
            derivative = new int[PulseWaveField.Length];
            for (int i = 0; i < PulseWaveField.Length - 1; i++)
                derivative[i + 1] = PulseWaveField[i + 1] - PulseWaveField[i];
            derivative[0] = derivative[1];
            this._repers = new int[5];
            ushort max = PulseWaveField.Max();
            for (int i = 0; i < PulseWaveField.Length; i++)
            {
                if (PulseWaveField[i] == max)
                    this._repers[0] = i;
            }
            this._repers[1] = Math.Min(this._repers[0] + 22, PulseWaveField.Length - 1);

            int[] repers = new int[(PulseWaveField.Length - this._repers[1]) / 2];
            for (int i = 0; i < repers.Length; i++)
                repers[i] = this._repers[1] + 1 + i;
            for (int i = 0; i < repers.Length; i++)
                for (int j = i + 1; j < repers.Length; j++)
                    if (Math.Abs(derivative[repers[j]]) < Math.Abs(derivative[repers[i]]))
                    {
                        int temp = repers[i];
                        repers[i] = repers[j];
                        repers[j] = temp;
                    }
            repers = (int[])repers.Take(20).ToArray();
            this._repers[2] = 0;
            this._repers[3] = 0;
            int count2 = 0, count3 = 0, srednee = 0;
            for (int i = 0; i < 20 && i < repers.Length; i++)
                srednee += repers[i];
            if (repers.Length > 0)
                srednee /= Math.Min(20, repers.Length);
            for (int i = 0; i < 20 && i < repers.Length; i++)
            {
                if (repers[i] < srednee)
                {
                    this._repers[2] += repers[i];
                    count2++;
                }
                else
                {
                    this._repers[3] += repers[i];
                    count3++;
                }
            }
            if (count2 != 0)
                this._repers[2] /= count2;
            if (count3 != 0)
                this._repers[3] /= count3;

            for (int i = this._repers[1] + 1; i < this._repers[3]; i++)
                if (PulseWaveField[i] < PulseWaveField[this._repers[2]])
                    this._repers[2] = i;
            for (int i = this._repers[2] + 1; i < PulseWaveField.Length; i++)
                if (PulseWaveField[i] > PulseWaveField[this._repers[3]])
                    this._repers[3] = i;

            if (this._repers[3] - this._repers[2] < 5)
            {
                this._repers[2] = this._repers[1] + 11;
                this._repers[3] = this._repers[2] + 11;
            }

            this._repers[4] = Math.Max(this._repers[3] + 2, derivative.Length / 4 * 3 + 1);
            float minDer = float.MaxValue;
            for (int i = this._repers[4]; i < derivative.Length - 2 - 10; i++)
            {
                float sredDerivative = 0;
                for (int j = -1; j <= 1; j++)
                    sredDerivative += Math.Abs(derivative[i + j]);
                sredDerivative /= 3;
                if (sredDerivative <= minDer)
                {
                    this._repers[4] = i;
                    minDer = sredDerivative;
                }
            }
            if ((PulseWaveField.Length - 1) - this._repers[4] < 5)
                this._repers[4] = (PulseWaveField.Length - 1) - 11;
        }

        public void AutoRepers_OLD()
        {
            if (PulseWaveField == null) return;
            derivative = new int[PulseWaveField.Length];
            for (int i = 0; i < PulseWaveField.Length - 1; i++)
                derivative[i + 1] = PulseWaveField[i + 1] - PulseWaveField[i];
            derivative[0] = derivative[1];
            _repers = new int[6];
            _repers[0] = 0;
            _repers[5] = PulseWaveField.Length - 1;
            ushort max = PulseWaveField.Max();
            for (int i = 0; i < PulseWaveField.Length; i++)
            {
                if (PulseWaveField[i] == max)
                    _repers[1] = i;
            }
            int begin = (_repers[5] - _repers[1]) / 15,
                end = (_repers[5] - _repers[1]) / 10,
                analyzePeriod = (_repers[5] - _repers[1] - begin - end) / 3;
            for (int i = 0; i < 3; i++)
            {
                _repers[i + 2] = _repers[1] + begin + analyzePeriod * i;
                int maxDerivative = derivative[_repers[i + 2]];
                for (int j = _repers[1] + begin + analyzePeriod * i + 1; j < _repers[1] + begin + analyzePeriod * (i + 1); j++)
                    if (derivative[j] > maxDerivative)
                    {
                        _repers[i + 2] = j;
                        maxDerivative = derivative[j];
                    }
            }
        }

        //*****************************************************************
        //                            Расчеты                             *
        //*****************************************************************

        public string ResultStrP5P2 { get; private set; }
        public string ResultStrKp { get; private set; }
        public string ResultStrKn { get; private set; }
        public int ResultTopPressure { get; private set; }
        public int ResultLowerPressure { get; private set; }

        //sex = true = m  sex = false = w
        public void MakeCalc(bool sex, int age, float growth, int weight,
            float j = Statics.J, float n = Statics.N, float h = Statics.H, float i = Statics.I, float r = Statics.R)
        {
            if (_repers == null || _repers.Length < 5)
                return;
            float s = (sex ? 1 : 0.96f),
                  b = DiscretesToMillimeters(Math.Abs(_repers[3] - _repers[0]));
            ResultStrP5P2 = "A4 - A1 = " + Math.Abs(_repers[3] - _repers[0]) + " дискрет(ы) = " + PrintStr(b) +
                            " в \"Федянинских\" мм.";
            ResultStrKp = "Kp = A3/A4 = " + PulseWaveField[_repers[2]] + "/" + PulseWaveField[_repers[3]] + " = ";
            ResultStrKn = "Kn = (" + age + "/54,5)^" + PrintStr(j) + "*(" + weight + "/(" + growth.ToString(CultureInfo.InvariantCulture) + "-100)^" + PrintStr(n) +
                "*" + PrintStr(s) + "^" + PrintStr(h) + "*(14/" + PrintStr(b) + ")^" + PrintStr(i);
            double polynom = Math.Pow(age / 54.5, j),
                   kN = polynom;
            polynom = Math.Pow((double)weight / (growth - 100), n);
            kN *= polynom;
            polynom = Math.Pow(s, h);
            kN *= polynom;
            polynom = Math.Pow(14.0 / b, i);
            kN *= polynom;
            polynom = (double)PulseWaveField[_repers[2]] / PulseWaveField[_repers[3]];
            ResultStrKp += PrintStr(polynom) + ".";
            ResultStrKn += "*(1,43*" + PrintStr(polynom) + ")^" + PrintStr(r) + " = ";
            polynom = Math.Pow(1.43 * polynom, r);
            kN *= polynom;
            ResultStrKn += PrintStr(kN) + ".";
            ResultTopPressure = (int)Math.Round(120 * kN);
            if (ResultTopPressure > 120)
                ResultLowerPressure = (int)Math.Round(150 * (1 - Math.Exp(-(ResultTopPressure - 41.3) / (310.0 / 3))));
            //ResultLowerPressure = (int)Math.Round(105*(1 - Math.Exp(- ResultTopPressure/(200.0/3.0))));
            else
                ResultLowerPressure = (int)Math.Round(ResultTopPressure * 2.0 / 3.0);
        }

        // переводим дискреты в "Федянинские" миллиметры
        private static float DiscretesToMillimeters(int dicretes)
        {
            return dicretes * 5.8f / 22f;
            //return dicretes * 6.14f / 22f;
        }

        private static string PrintStr(float val)
        {
            return val.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
        }

        private static string PrintStr(double val)
        {
            return val.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
        }
    }
}