﻿using System;
using System.Collections;
using System.Linq;
using System.Globalization;

namespace TonParser._Math
{
    [Serializable]
    internal class PulseWaveTags
    {

        /// <summary>
        /// Данные с файла
        /// </summary>
        private ushort[] pulseData { get; }
        public ushort[] PulseWaveField { get; set; }
        public int StartDelay { get; set; }
        [Serializable] //для DeepClone()
        private struct Reper //уничтожить эту структуру
        {
            public int Position;
            public bool Visible;
        }
        private double frequency { get; }
        private bool flagRepersFixed = false;  // флаг, что реперы уже установлены
        private int[] derivative;   // производная
        private Reper[] repers;
        // метки
        private ArrayList _arrChina, // китайские точки (максимум производной)
            _arrMax,                 // максимум сигнала
            _arrBegin;               // начало кардиоцикла

        // масштаб давления (повышение давления на 1 дискрету)
        private int _top, _lower, _maxValAll = 0;
        private double _scalePress;

        public int[] Repers
        {
            get
            {
                if (repers == null)
                    return null;

                var temp = new int[repers.Length];

                for (int i = 0; i < repers.Length; i++)
                    temp[i] = repers[i].Position;

                return temp;
            }
            set
            {
                if (value == null)
                    return;

                if (repers == null || repers.Length < value.Length)
                    repers = new Reper[value.Length];

                for (int i = 0; i < value.Length; i++)
                    repers[i].Position = value[i];
            }
        }

        public PulseWaveTags(ushort[] pulseData, double frequency)
        {
            this.pulseData = pulseData;
            this.repers = new Reper[5];
            this.frequency = frequency;
            DoTagsPulseWave();
            CalculatePulseWave();
        }

        public int GetReper(int i = 1)
        {
            return repers != null && i >= 1 && i <= repers.Length ? repers[i - 1].Position : 0;
        }

        public float GetRepersSeconds(int a1 = 0, int a2 = 0)
        {
            if (pulseData == null || pulseData.Length <= 0 || repers == null || repers.Length <= 0) return 0;

            if (a1 <= 0 || a1 > repers.Length)
                a1 = 0;
            else
                a1 = repers[a1 - 1].Position;

            if (a2 <= 0 || a2 > repers.Length)
                a2 = PulseWaveField.Length - 1;
            else
                a2 = repers[a2 - 1].Position;

            return Math.Abs(a2 - a1) * 20f / pulseData.Length;
        }

        // разметка ПВ
        public void DoTagsPulseWave()
        {
            FindChina();
            _arrMax = new ArrayList();
            _arrBegin = new ArrayList();

            if (_arrChina.Count > 1)
            {
                int period = 0;
                for (int i = 1; i < _arrChina.Count; i++)
                    period += (int)_arrChina[i] - (int)_arrChina[i - 1];
                period /= _arrChina.Count - 1;
                for (int i = 0; i < _arrChina.Count; i++)
                {
                    int curChina = (int)_arrChina[i], iMin = curChina - 12, iMax = curChina;

                    for (int j = ((curChina - period / 2) > 0 ? (curChina - period / 2) : 0);
                        j < ((curChina + period / 2) < pulseData.Length ? (curChina + period / 2) : pulseData.Length);
                        j++)
                    {
                        if (pulseData[j] > pulseData[iMax])
                            iMax = j;
                    }
                    _arrBegin.Add(iMin);
                    _arrMax.Add(iMax);
                }
            }
        }
        // поиск китайских точек
        private void FindChina()
        {
            _arrChina = new ArrayList();
            derivative = new int[pulseData.Length];
            for (int i = 0; i < pulseData.Length - 1; i++)
                derivative[i + 1] = pulseData[i + 1] - pulseData[i];
            derivative[0] = derivative[1];
            int curIndex = 2;
            while (curIndex < derivative.Length - 2)
            {
                int maxVal = (derivative[curIndex - 1] + derivative[curIndex] +
                    derivative[curIndex + 1]) / 3, maxIndex = curIndex;
                for (int i = 1; (i <= 80) && (curIndex < derivative.Length - 2); i++)
                {
                    curIndex++;
                    int curVal = 0;
                    for (int j = -1; j <= 1; j++)
                        curVal += derivative[curIndex + j];
                    curVal /= 3;
                    if (curVal > maxVal)
                    {
                        maxVal = curVal;
                        maxIndex = curIndex;
                    }
                }
                _arrChina.Add(maxIndex);
            }

            bool flagElementDeleted;
            do
            {
                flagElementDeleted = false;
                for (int i = 1; i < _arrChina.Count; i++)
                {
                    if (((int)_arrChina[i] - (int)_arrChina[i - 1]) <= 80)
                    {
                        _arrChina.RemoveAt(derivative[(int)_arrChina[i - 1]] > derivative[(int)_arrChina[i]] ? i : i - 1);
                        flagElementDeleted = true;
                        break;
                    }
                }
            } 
            while (flagElementDeleted);

            if ((_arrChina.Count > 0) && ((int)_arrChina[0] < 12))
                _arrChina.RemoveAt(0);

            if ((_arrChina.Count > 1) && (pulseData.Length - ((int)_arrChina[_arrChina.Count - 1]) < 10))
                _arrChina.RemoveAt(_arrChina.Count - 1);

            int sred = 0;
            for (int i = 0; i < _arrChina.Count; i++)
                sred += derivative[(int)_arrChina[i]];
            sred /= _arrChina.Count;
            do
            {
                flagElementDeleted = false;
                for (int i = 0; i < _arrChina.Count; i++)
                {
                    if (derivative[(int)_arrChina[i]] < sred / 5)
                    {
                        _arrChina.RemoveAt(i);
                        flagElementDeleted = true;
                        break;
                    }
                }
            } 
            while (flagElementDeleted);
        }

        public void CalculatePulseWave()
        {
            if (_arrBegin.Count >= 2)
            {
                int sredPeriod = 0;
                for (int i = 0; i < _arrBegin.Count - 1; i++)
                    sredPeriod += (int)_arrBegin[i + 1] - (int)_arrBegin[i];

                sredPeriod /= (_arrBegin.Count - 1);

                PulseWaveField = new ushort[sredPeriod];
                for (int i = 0; i < sredPeriod; i++)
                {
                    int sum = 0, sumCount = 0;
                    for (int j = 0; j < _arrBegin.Count - 1; j++)
                    {
                        int curIndex = (int)_arrBegin[j] + i;
                        if (curIndex < pulseData.Length && curIndex < (int)_arrBegin[j + 1])
                        {
                            sum += pulseData[curIndex];
                            sumCount++;
                        }
                    }
                    PulseWaveField[i] = (ushort)(sum / sumCount);
                }
                ushort min = PulseWaveField.Min(), max = PulseWaveField.Max();

                for (int i = 0; i < PulseWaveField.Length; i++)
                {
                    if (!flagRepersFixed && PulseWaveField[i] == max)
                        repers[0].Position = i;
                    PulseWaveField[i] -= min;
                }
                if (!flagRepersFixed)
                    repers[1].Position = Math.Min(repers[0].Position + 22, PulseWaveField.Length - 1);

                if ((repers[0].Position < PulseWaveField.Length) && (repers[1].Position < PulseWaveField.Length))
                    Statics.a2a1 = (float)PulseWaveField[repers[1].Position] / PulseWaveField[repers[0].Position]; // А2 / А1

                double waveSeconds = PulseWaveField.Length / frequency;
                Statics.fss = (int)Math.Round(60d / waveSeconds); //пульс
            }
        }
        // определения индекса состояния сосудов
        public int GetSosud(float a2a1, float s1 = Statics._patientS1, float s2 = Statics._patientS2)
        {
            if (a2a1 < s1)
                return 1;
            else if (a2a1 > s2)
                return 3;
            else
                return 2;
        }

        // стрессовый индекс
        public int GetStress()
        {
            byte k = 0;
            if (_arrBegin.Count >= 2)
            {
                int sredPeriod = 0, minPeriod = 0, maxPeriod = 0, curPeriod;
                for (int i = 0; i < _arrBegin.Count - 1; i++)
                {
                    curPeriod = (int)_arrBegin[i + 1] - (int)_arrBegin[i];
                    sredPeriod += curPeriod;
                    if ((curPeriod < minPeriod) || (i == 0))
                        minPeriod = curPeriod;
                    if ((curPeriod > maxPeriod) || (i == 0))
                        maxPeriod = curPeriod;
                }
                sredPeriod /= (_arrBegin.Count - 1);
                double s = Math.Min(100.0 * (maxPeriod - minPeriod) / sredPeriod, 30.0);
                k = (byte)Math.Round(5 - s / 6.0);
            }
            return k;
        }

        public int CalcAge()
        {
            if (PulseWaveField == null || repers == null || repers.Length < 4 ||
                repers[2].Position >= PulseWaveField.Length || repers[3].Position >= PulseWaveField.Length)
                return 0;
            float val = (float)PulseWaveField[repers[2].Position] / PulseWaveField[repers[3].Position];
            if (val < 0.5)
            {
                int res = CalcAge(15, 25, 0, 0.5f, val);
                return (res >= 20 ? res : 20);
            }
            else if (val >= 0.5 && val <= 0.8)
                return CalcAge(25, 35, 0.5f, 0.8f, val);
            else if (val > 0.8 && val <= 0.91)
                return CalcAge(35, 45, 0.8f, 0.91f, val);
            else if (val > 0.91 && val <= 1.1)
                return CalcAge(45, 55, 0.91f, 1.1f, val);
            else if (val > 1.1)
            {
                int res = CalcAge(55, 80, 1.1f, 1.7f, val);
                return (res <= 80 ? res : 80);
            }
            return 50;
        }

        private int CalcAge(int age1, int age2, float p1, float p2, float p)
        {
            return (int)Math.Round(Math.Abs(p - p1) * (age2 - age1) / (p2 - p1)) + age1;
        }

        public int CalcBlood(float k1, out Double val)
        {
            if (PulseWaveField == null || repers == null || repers.Length < 2 ||
                repers[0].Position >= PulseWaveField.Length || repers[1].Position >= PulseWaveField.Length)
            {
                val = 0;
                return 0;
            }
            val = k1 * Math.Pow((Double)PulseWaveField[repers[1].Position] / PulseWaveField[repers[0].Position], -0.3) *
                  Math.Pow(Statics.fss / 80d, 2);
            int koef = (int)Math.Round(75 * val);
            if (koef < 60)
                return 60;
            if (koef > 90)
                return 90;
            return koef;
        }

        public int CalcBlood(float k1)
        {
            Double tempBlood;
            return CalcBlood(k1, out tempBlood);
        }

        public double PolynomTop(float a2a1, int fss, int fssopor = Statics._patientFssOpor, float k = Statics._patientK, float a = Statics._patientA, float b = Statics._patientB)
        {
            double z = Math.Pow(a2a1, a) * Math.Pow((float)fss / fssopor, b);
            return 1 - Math.Exp(-k * z);
        }

        public double PolynomLower(float a2a1, int fss, int fssopor = Statics._patientFssOpor, float k = Statics._patientK, float c = Statics._patientC, float d = Statics._patientD)
        {
            double z = Math.Pow(a2a1, c) * Math.Pow((float)fss / fssopor, -d);
            return 1 - Math.Exp(-k * z);
        }

        // расчет АД по средней ПВ
        public byte CalculateADUpper(float a2a1, int fss, float k1, 
            int fssopor = Statics._patientFssOpor, float k = Statics._patientK,
            float a = Statics._patientA, float b = Statics._patientB)
        {
            // top
            double polynom = PolynomTop(a2a1, fss, fssopor, k, a, b);
            return (byte)Math.Round(230d / k1 * polynom);
        }

        public byte CalculateADLower(float a2a1, int fss, float k2,
            int fssopor = Statics._patientFssOpor, float k = Statics._patientK,
            float c = Statics._patientC, float d = Statics._patientD)
        {
            //lower
            double polynom = PolynomLower(a2a1, fss, fssopor, k, c, d);
            return (byte)Math.Round(230d / k2 * polynom);
        }

        public void SetStartDelay(float seconds)
        {
            if (pulseData == null || pulseData.Length <= 0)
            {
                StartDelay = 0;
                return;
            }

            StartDelay = (int)Math.Round(seconds * pulseData.Length / 20f);
        }

        // расчет масштаба давления (повышение давления на 1 дискрету)
        public void CalcScalePress(int top, int lower, int maxAll = 0)
        {
            if (PulseWaveField == null || PulseWaveField.Length <= 0)
                return;

            _top = top;
            _lower = lower;
            _maxValAll = Math.Max(PulseWaveField.Max(), maxAll);
            int diff = _maxValAll - PulseWaveField.Min();

            _scalePress = diff != 0 ? (double)(top - lower) / diff : 0;
        }

        // конвертация в АД
        private double ConvertToPress(int i)
        {
            if (PulseWaveField == null || PulseWaveField.Length <= i)
                return 0d;

            var min = PulseWaveField.Min();
            return _lower + (PulseWaveField[i] - min) * _scalePress;
        }

        public double[] ConvertToPress()
        {
            if (PulseWaveField == null || PulseWaveField.Length <= 0)
                return null;

            var result = new double[PulseWaveField.Length];
            for (int i = 0; i < PulseWaveField.Length; i++)
                result[i] = ConvertToPress(i);

            return result;
        }
    }
}
