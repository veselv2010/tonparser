﻿//
//   Класс PulseWaveField для обработки пульсовой волны.
// Чтение/запись в файл, прорисовка, выделение отдельных волн, разметка.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;

namespace TonParser._Math
{
    [Serializable]
    public class PulseWave
    {
        //*****************************************************************
        //                         Базовые поля                           *
        //*****************************************************************

        [Serializable]
        private struct Reper
        {
            public int Position;
            public bool Visible;
        }
        public ushort[] PulseWaveField { get; set; } //сериализованная волна
        private ushort[] _pulseData; // волна (чистые данные)
        private string _fileName;    // имя файла
        private int[] _derivative;   // производная
        // параметры средней волны
        private Reper[] _repers;     // реперы
        private bool _flagRepersFixed = false;  // флаг, что реперы уже установлены
        private float _a2A1;         // отношение А2 / А1
        private int _fss;            // ЧСС (частота сердечных сокращений, уд в мин)

        // метки
        private ArrayList _arrChina, // китайские точки (максимум производной)
            _arrMax,                 // максимум сигнала
            _arrBegin;               // начало кардиоцикла

        // доступ к полям
        public int GetPulseLength() { return _pulseData.Length; }
        public string GetFileNameShort() { return Path.GetFileName(_fileName); }
        public int GetReper(int i = 1)
        {
            return _repers != null && i >= 1 && i <= _repers.Length ? _repers[i - 1].Position : 0;
        }
        public int[] Repers
        {
            get
            {
                if (_repers == null)
                    return null;
                var temp = new int[_repers.Length];
                for (int i = 0; i < _repers.Length; i++)
                    temp[i] = _repers[i].Position;
                return temp;
            }
            set
            {
                if (value == null)
                    return;
                if (_repers == null || _repers.Length < value.Length)
                    _repers = new Reper[value.Length];
                for (int i = 0; i < value.Length; i++)
                    _repers[i].Position = value[i];
                _flagRepersFixed = true;
            }
        }
        public string RepersLabel { get; set; }
        public float PulseA2A1
        {
            get { return Statics.a2a1; }
            set { Statics.a2a1 = value; }
        }
        public int PulseFSS
        {
            get { return Statics.fss; }
            set { Statics.fss = value; }
        }

        // частота (число замеров в секунду)
        public Double Frequency
        {
            get { return (_pulseData != null ? _pulseData.Length / 20d : 0); }
        }

        // задержка в начале массива
        public int StartDelay { get; private set; }

        // режим отображения реперов
        private bool _bufferMode;

        // масштаб давления (повышение давления на 1 дискрету)
        private int _top, _lower, _maxValAll = 0;
        private double _scalePress;


        //*****************************************************************
        //                         Конструкторы                           *
        //*****************************************************************

        public PulseWave(bool bufferMode = true)
        {
            _bufferMode = bufferMode;
            _repers = new Reper[5];
            SetRepersVisible();
            RepersLabel = "A";
        }

        public PulseWave(int len, bool bufferMode = true) : this(bufferMode)
        {
            _pulseData = new ushort[len];
        }

        public PulseWave(ushort[] data, bool bufferMode = true) : this(bufferMode)
        {
            _pulseData = data;
        }
        
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }


        //*****************************************************************
        //             Методы для базовой работы с ПВ                     *
        //*****************************************************************

        // очистка установленных реперов
        public void ClearRepers()
        {
            _flagRepersFixed = false;
        }

        // установка видимости реперов
        public void SetRepersVisible(bool[] flags = null)
        {
            if (flags == null)
            {
                flags = new bool[] { true, _bufferMode, false, !_bufferMode, false };
            }
            //flags = new bool[] { true, true, true, true, true }; // для теста, чтобы все реперы были видны

            if (_repers == null || _repers.Length < 5)
                _repers = new Reper[5];
            for (int i = 0; i < _repers.Length; i++)
            {
                if (flags.Length <= i)
                    _repers[i].Visible = false;
                else
                    _repers[i].Visible = flags[i];
            }
        }

        // расчет времени между реперами
        public float GetRepersSeconds(int a1 = 0, int a2 = 0)
        {
            if (PulseWaveField == null || PulseWaveField.Length <= 0 || _pulseData == null || _pulseData.Length <= 0 || _repers == null || _repers.Length <= 0) return 0;

            if (a1 <= 0 || a1 > _repers.Length)
                a1 = 0;
            else
                a1 = _repers[a1 - 1].Position;

            if (a2 <= 0 || a2 > _repers.Length)
                a2 = PulseWaveField.Length - 1;
            else
                a2 = _repers[a2 - 1].Position;

            return Math.Abs(a2 - a1) * 20f / _pulseData.Length;
        }

        // установка паузы в начале (задержки)
        public void SetStartDelay(float seconds)
        {
            if (_pulseData == null || _pulseData.Length <= 0)
            {
                StartDelay = 0;
                return;
            }

            StartDelay = (int)Math.Round(seconds * _pulseData.Length / 20f);
        }

        // суммарный анализ волны
        public void AnalyzePulseWave()
        {
            DoTagsPulseWave();
            CalculatePulseWave();
        }

        // разметка ПВ
        private void DoTagsPulseWave()
        {
            FindChina();
            _arrMax = new ArrayList();
            _arrBegin = new ArrayList();

            if (_arrChina.Count > 1)
            {
                int period = 0;
                for (int i = 1; i < _arrChina.Count; i++)
                    period += (int)_arrChina[i] - (int)_arrChina[i - 1];
                period /= _arrChina.Count - 1;
                for (int i = 0; i < _arrChina.Count; i++)
                {
                    int curChina = (int)_arrChina[i], iMin = curChina - 12, iMax = curChina;

                    for (int j = ((curChina - period / 2) > 0 ? (curChina - period / 2) : 0);
                        j < ((curChina + period / 2) < _pulseData.Length ? (curChina + period / 2) : _pulseData.Length);
                        j++)
                    {
                        if (_pulseData[j] > _pulseData[iMax])
                            iMax = j;
                    }
                    _arrBegin.Add(iMin);
                    _arrMax.Add(iMax);
                }
            }
        }

        // поиск китайских точек
        public void FindChina()
        {
            _arrChina = new ArrayList();
            _derivative = new int[_pulseData.Length];
            for (int i = 0; i < _pulseData.Length - 1; i++)
                _derivative[i + 1] = _pulseData[i + 1] - _pulseData[i];
            _derivative[0] = _derivative[1];
            int curIndex = 2;
            while (curIndex < _derivative.Length - 2)
            {
                int maxVal = (_derivative[curIndex - 1] + _derivative[curIndex] +
                    _derivative[curIndex + 1]) / 3, maxIndex = curIndex;
                for (int i = 1; (i <= 80) && (curIndex < _derivative.Length - 2); i++)
                {
                    curIndex++;
                    int curVal = 0;
                    for (int j = -1; j <= 1; j++)
                        curVal += _derivative[curIndex + j];
                    curVal /= 3;
                    if (curVal > maxVal)
                    {
                        maxVal = curVal;
                        maxIndex = curIndex;
                    }
                }
                _arrChina.Add(maxIndex);
            }

            bool flagElementDeleted;
            do
            {
                flagElementDeleted = false;
                for (int i = 1; i < _arrChina.Count; i++)
                {
                    if (((int)_arrChina[i] - (int)_arrChina[i - 1]) <= 80)
                    {
                        _arrChina.RemoveAt(_derivative[(int)_arrChina[i - 1]] > _derivative[(int)_arrChina[i]] ? i : i - 1);
                        flagElementDeleted = true;
                        break;
                    }
                }
            } while (flagElementDeleted);
            if ((_arrChina.Count > 0) && ((int)_arrChina[0] < 12))
                _arrChina.RemoveAt(0);
            if ((_arrChina.Count > 1) && (_pulseData.Length - ((int)_arrChina[_arrChina.Count - 1]) < 10))
                _arrChina.RemoveAt(_arrChina.Count - 1);
            int sred = 0;
            for (int i = 0; i < _arrChina.Count; i++)
                sred += _derivative[(int)_arrChina[i]];
            sred /= _arrChina.Count;
            do
            {
                flagElementDeleted = false;
                for (int i = 0; i < _arrChina.Count; i++)
                {
                    if (_derivative[(int)_arrChina[i]] < sred / 5)
                    {
                        _arrChina.RemoveAt(i);
                        flagElementDeleted = true;
                        break;
                    }
                }
            } while (flagElementDeleted);
        }

        // проверка, что метка китайской точки входит в диапазон
        public int ChinaInDiapazon(int start, int end)
        {
            for (int i = 0; i < _arrChina.Count; i++)
                if ((start < (int)_arrChina[i]) && ((int)_arrChina[i] < end))
                    return (int)_arrChina[i];
            return -1;
        }

        // определение средней волны
        private void CalculatePulseWave()
        {
            if (_arrBegin.Count >= 2)
            {
                int sredPeriod = 0;
                for (int i = 0; i < _arrBegin.Count - 1; i++)
                    sredPeriod += (int)_arrBegin[i + 1] - (int)_arrBegin[i];
                sredPeriod /= (_arrBegin.Count - 1);
                PulseWaveField = new ushort[sredPeriod];
                for (int i = 0; i < sredPeriod; i++)
                {
                    int sum = 0, sumCount = 0;
                    for (int j = 0; j < _arrBegin.Count - 1; j++)
                    {
                        int curIndex = (int)_arrBegin[j] + i;
                        if (curIndex < _pulseData.Length && curIndex < (int)_arrBegin[j + 1])
                        {
                            sum += _pulseData[curIndex];
                            sumCount++;
                        }
                    }
                    PulseWaveField[i] = (ushort)(sum / sumCount);
                }
                ushort min = PulseWaveField.Min(), max = PulseWaveField.Max();
                //if (!_flagRepersFixed)
                //    _repers = new Reper[5];
                for (int i = 0; i < PulseWaveField.Length; i++)
                {
                    if (!_flagRepersFixed && PulseWaveField[i] == max)
                        _repers[0].Position = i;
                    PulseWaveField[i] -= min;
                }
                if (!_flagRepersFixed)
                    _repers[1].Position = Math.Min(_repers[0].Position + 22, PulseWaveField.Length - 1);
                // А2 / А1
                if ((_repers[0].Position < PulseWaveField.Length) && (_repers[1].Position < PulseWaveField.Length))
                    Statics.a2a1 = (float)PulseWaveField[_repers[1].Position] / PulseWaveField[_repers[0].Position];
                //this.FSS = (int)Math.Round(3600f / ((pulseWave.Length - 1) * (60f / 220f)));
                double waveSeconds = PulseWaveField.Length / Frequency;
                Statics.fss = (int)Math.Round(60d / waveSeconds);
            }
        }

        // расчет АД по средней ПВ
        public void CalculateAD(float a2a1, int fss, int fssopor,
            float k1, float k2, float k,
            float a, float b, float c, float d,
            out byte topP, out byte lowerP)
        {
            // top
            double polynom = PolynomTop(a2a1, fss, fssopor, k, a, b);
            topP = (byte)Math.Round(230d / k1 * polynom);
            // lower
            polynom = PolynomLower(a2a1, fss, fssopor, k, c, d);
            lowerP = (byte)Math.Round(230d / k2 * polynom);
        }

        public void CalculateAD(int fssopor,
            float k1, float k2, float k,
            float a, float b, float c, float d,
            out byte topP, out byte lowerP)
        {
            // top
            double polynom = PolynomTop(_a2A1, _fss, fssopor, k, a, b);
            topP = (byte)Math.Round(230d / k1 * polynom);
            // lower
            polynom = PolynomLower(_a2A1, _fss, fssopor, k, c, d);
            lowerP = (byte)Math.Round(230d / k2 * polynom);
        }

        public static double PolynomTop(float a2a1, int fss, int fssopor, float k, float a, float b)
        {
            double z = Math.Pow(a2a1, a) * Math.Pow((float)fss / fssopor, b);
            return 1 - Math.Exp(-k * z);
        }

        public static double PolynomLower(float a2a1, int fss, int fssopor, float k, float c, float d)
        {
            double z = Math.Pow(a2a1, c) * Math.Pow((float)fss / fssopor, -d);
            return 1 - Math.Exp(-k * z);
        }

        // определения индекса состояния сосудов
        public int GetSosud(float a2a1, float s1, float s2)
        {
            if (a2a1 < s1)
                return 1;
            else if (a2a1 > s2)
                return 3;
            else
                return 2;
        }

        public int GetSosud(float s1, float s2)
        {
            if (_a2A1 < s1)
                return 1;
            else if (_a2A1 > s2)
                return 3;
            else
                return 2;
        }

        // стрессовый индекс
        public int GetStress()
        {
            byte k = 0;
            if (_arrBegin.Count >= 2)
            {
                int sredPeriod = 0, minPeriod = 0, maxPeriod = 0, curPeriod;
                for (int i = 0; i < _arrBegin.Count - 1; i++)
                {
                    curPeriod = (int)_arrBegin[i + 1] - (int)_arrBegin[i];
                    sredPeriod += curPeriod;
                    if ((curPeriod < minPeriod) || (i == 0))
                        minPeriod = curPeriod;
                    if ((curPeriod > maxPeriod) || (i == 0))
                        maxPeriod = curPeriod;
                }
                sredPeriod /= (_arrBegin.Count - 1);
                double s = Math.Min(100.0 * (maxPeriod - minPeriod) / sredPeriod, 30.0);
                k = (byte)Math.Round(5 - s / 6.0);
            }
            return k;
        }

        // конвертация в АД
        public double ConvertToPress(int i)
        {
            if (PulseWaveField == null || PulseWaveField.Length <= i)
                return 0d;

            var min = PulseWaveField.Min();
            return _lower + (PulseWaveField[i] - min) * _scalePress;
        }

        public double[] ConvertToPress()
        {
            if (PulseWaveField == null || PulseWaveField.Length <= 0)
                return null;

            var result = new double[PulseWaveField.Length];
            for (int i = 0; i < PulseWaveField.Length; i++)
                result[i] = ConvertToPress(i);

            return result;
        }

        // расчет масштаба давления (повышение давления на 1 дискрету)
        public void CalcScalePress(int top, int lower, int maxAll = 0)
        {
            if (PulseWaveField == null || PulseWaveField.Length <= 0)
                return;

            _top = top;
            _lower = lower;
            _maxValAll = Math.Max(PulseWaveField.Max(), maxAll);
            int diff = _maxValAll - PulseWaveField.Min();

            _scalePress = diff != 0 ? (double)(top - lower) / diff : 0;
        }


        //*****************************************************************
        //                   Чтение и запись в файл                       *
        //*****************************************************************

        /* ----- сохранить ПВ в текстовый файл ----- */
        public bool SavePulseToTextFile(string strFileName, ref string strError)
        {
            try
            {
                using (StreamWriter myStreamWriter = new StreamWriter(strFileName, false))
                {
                    for (int i = 0; i < _pulseData.Length; i++)
                        myStreamWriter.Write(_pulseData[i].ToString() + " ");
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        /* ----- чтение ПВ из текстового файла ----- */
        public bool LoadPulseFromTextFile(string strFileName, ref string strError)
        {
            try
            {
                using (StreamReader myStreamReader = File.OpenText(strFileName))
                {
                    string line;
                    while ((line = myStreamReader.ReadLine()) != null)
                    {
                        string[] numbers = line.Split(' ');
                        List<ushort> lstNumbers = new List<ushort>();
                        foreach (string number in numbers)
                        {
                            if (!string.IsNullOrEmpty(number) && !string.IsNullOrEmpty(number.Trim()))
                            {
                                ushort val;
                                if (!ushort.TryParse(number, out val))
                                    throw new Exception("Строка '" + number + "' не является числом.");
                                lstNumbers.Add(val);
                            }
                        }
                        _pulseData = lstNumbers.ToArray();
                    }
                }
                _fileName = strFileName;
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                _pulseData = null;
                return false;
            }
        }

        /* ----- сохранить ПВ в двоичный файл ----- */
        public bool SavePulseToBinFile(string strFileName, ref string strError)
        {
            try
            {
                using (FileStream myFileStream = new FileStream(strFileName, FileMode.Create, FileAccess.Write))
                {
                    using (BinaryWriter myBinaryWriter = new BinaryWriter(myFileStream))
                    {
                        for (int i = 0; i < _pulseData.Length; i++)
                            myBinaryWriter.Write(_pulseData[i]);
                    }
                }
                return true;
            }
            catch (Exception ioex)
            {
                strError = ioex.Message;
                return false;
            }
        }

        /* ----- открыть ПВ из двоичного файла ----- */
        public bool LoadPulseFromBinFile(string strFileName, ref string strError)
        {
            try
            {
                using (FileStream myFileStream = new FileStream(strFileName, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader myBinaryReader = new BinaryReader(myFileStream))
                    {
                        _pulseData = new ushort[myFileStream.Length / 2];
                        int counter = 0;
                        while ((myFileStream.Position < myFileStream.Length) && (counter < _pulseData.Length))
                        {
                            _pulseData[counter] = myBinaryReader.ReadUInt16();
                            counter++;
                        }
                    }
                }
                _fileName = strFileName;
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                _pulseData = null;
                return false;
            }
        }


        //*****************************************************************
        //                           Графика                              *
        //*****************************************************************

        // вся пульсовая волна с производной
        public void DrawPulseWaveDataAndDerivative(Graphics gr, Rectangle rct, out int scroll,
            bool tags = false, double scale = 1, bool points = false, bool print = false, bool derivate = false)
        {
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gr.Clear(Color.White);
            if (_pulseData == null || _pulseData.Length <= 0)
            {
                scroll = 0;
                return;
            }
            Point[] pulsePoints = new Point[(int)Math.Min(Math.Round(rct.Width / scale),
                    Math.Max(Math.Round(_pulseData.Length - rct.Left / scale), 0))],
                derivativePoints = new Point[pulsePoints.Length];
            using (Pen penPulse = new Pen(Color.Black, 2),
                       penTagsBegin = new Pen(Color.Red),
                       penTagsMax = new Pen(Color.Blue),
                       penTagsChina = new Pen(Color.Green),
                       penDerivative = new Pen(Color.Gray),
                       penTags = new Pen(Color.LightGray),
                       penLines = new Pen(Color.DimGray))
            {
                using (Font fontWaves = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                            fontSecondsBig = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                            fontSecondsSmall = new Font("Microsoft Sans Serif", 8, FontStyle.Regular))
                {
                    const string strAxis = "сек";
                    const int axisFree = 30;
                    int axisWidth = axisFree + (int)Math.Ceiling(gr.MeasureString(strAxis, fontSecondsBig).Width),
                        axisHeight = (int)fontSecondsBig.GetHeight(gr) + 2,
                        derivateHeight = (derivate ? 50 : 0);
                    uint allH = (uint)(rct.Height - 20 - axisHeight - (derivate ? derivateHeight + 10 : 0));
                    uint min = _pulseData.Min(), max = _pulseData.Max();
                    int minD = _derivative.Min(), maxD = _derivative.Max();
                    int derivateLevel = rct.Bottom - axisHeight - 5 -
                                        Math.Max(0, Math.Abs(minD * derivateHeight / (maxD - minD)));
                    int iPoint = 0;
                    double curX = 0;
                    while ((curX < rct.Width) && (iPoint + (int)(rct.Left / scale) < _pulseData.Length) &&
                           (iPoint >= 0) && (iPoint < pulsePoints.Length))
                    {
                        int iIndex = iPoint + (int)(Math.Round(rct.Left / scale));
                        uint curH = 0;
                        if (iIndex < _pulseData.Length)
                            curH = (uint)(_pulseData[iIndex] - min);
                        Point curPoint = new Point((int)curX,
                                                   (int)
                                                   (rct.Bottom - axisHeight - derivateHeight - 10 - curH * allH / (max - min))),
                              curPointD = new Point((int)curX,
                                                    (int)
                                                    (derivateLevel - _derivative[iIndex] * derivateHeight / (maxD - minD)));
                        pulsePoints[iPoint] = curPoint;
                        derivativePoints[iPoint] = curPointD;
                        // разметка
                        if (tags)
                        {
                            if (_arrBegin.Contains(iIndex))
                            {
                                gr.DrawLine(penTagsBegin, new Point(curPoint.X, curPoint.Y - 10),
                                            new Point(curPoint.X, rct.Bottom - axisHeight));
                                string strPulse = (_arrBegin.IndexOf(iIndex) + 1).ToString();
                                gr.DrawString(strPulse, fontWaves,
                                              new SolidBrush(penTagsBegin.Color),
                                              new Point(
                                                  curPoint.X - (int)gr.MeasureString(strPulse, fontWaves).Width / 2,
                                                  rct.Top - 1));
                            }
                            if (_arrMax.Contains(iIndex))
                                gr.DrawLine(penTagsMax, new Point(curPoint.X, curPoint.Y - 10),
                                            new Point(curPoint.X, rct.Bottom - axisHeight));
                            if (_arrChina.Contains(iIndex))
                                gr.DrawLine(penTagsChina, new Point(curPoint.X, curPoint.Y - 10),
                                            new Point(curPoint.X, rct.Bottom - axisHeight));
                        }
                        // значения
                        if (print)
                        {
                            Font fontTextData = new Font("Microsoft Sans Serif", 6);
                            if (derivate)
                            {
                                gr.DrawString(_derivative[iIndex].ToString(), fontTextData,
                                              new SolidBrush(penDerivative.Color),
                                              new Point(curPointD.X, curPointD.Y + 2));
                            }
                            gr.DrawString(_pulseData[iIndex].ToString(), fontTextData, new SolidBrush(penPulse.Color),
                                          new Point(curPoint.X, curPoint.Y + 2));
                        }
                        curX += scale;
                        iPoint++;
                    }

                    // точки
                    if (points)
                    {
                        if (derivate)
                        {
                            foreach (var derivativePoint in derivativePoints)
                                gr.FillEllipse(new SolidBrush(penDerivative.Color), derivativePoint.X - 2,
                                               derivativePoint.Y - 2, 4, 4);
                        }
                        foreach (var pulsePoint in pulsePoints)
                            gr.FillEllipse(new SolidBrush(penPulse.Color), pulsePoint.X - 3, pulsePoint.Y - 3, 6, 6);
                    }
                    // оси
                    int axisLevel = rct.Bottom - axisHeight;
                    gr.DrawLine(penLines, 0, axisLevel, (int)curX + axisFree, axisLevel);
                    gr.DrawLine(penLines, (int)curX + axisFree, axisLevel, (int)curX + axisFree - 8, axisLevel - 3);
                    gr.DrawLine(penLines, (int)curX + axisFree, axisLevel, (int)curX + axisFree - 8, axisLevel + 3);
                    gr.DrawString(strAxis, fontSecondsBig, Brushes.Black, (int)curX + axisFree, axisLevel + 2);
                    int axisX = -rct.Left,
                        axisBigSeparator = (int)Math.Round(_pulseData.Length / 20d * scale),
                        axisLastText = axisX;
                    while (axisX <= curX)
                    {
                        double val = ((rct.Left + axisX) / scale) * 20 / _pulseData.Length;
                        gr.DrawLine(penLines, axisX, axisLevel, axisX, axisLevel + 7);
                        string axisText = val.ToString("0");
                        gr.DrawString(axisText, fontSecondsBig, Brushes.Black, axisX, axisLevel + 2);
                        axisLastText = axisX + (int)gr.MeasureString(axisText, fontSecondsBig).Width * 2;
                        for (int i = 1; i < 10; i++)
                        {
                            int tempAxisX = axisX + (int)Math.Round(axisBigSeparator / 10f * i);
                            if (tempAxisX > curX)
                                break;
                            gr.DrawLine(penLines, tempAxisX, axisLevel, tempAxisX, axisLevel + 2);
                            if (tempAxisX > axisLastText)
                            {
                                val = ((rct.Left + tempAxisX) / scale) * 20 / _pulseData.Length;
                                axisText = val.ToString("0.#", CultureInfo.CurrentCulture);
                                if (tempAxisX + gr.MeasureString(axisText, fontSecondsSmall).Width < axisX + axisBigSeparator)
                                {
                                    gr.DrawString(axisText, fontSecondsSmall,
                                                  Brushes.Gray, tempAxisX, axisLevel + 3);
                                    axisLastText = tempAxisX + (int)gr.MeasureString(axisText, fontSecondsSmall).Width * 2;
                                }
                            }
                        }
                        axisX += axisBigSeparator;
                    }
                    // кривые
                    if (derivate)
                    {
                        gr.DrawLine(penTags, new Point(0, derivateLevel),
                                    new Point(rct.Width, derivateLevel));
                        gr.DrawCurve(penDerivative, derivativePoints, 1.0F);
                    }
                    if (pulsePoints.Length > 1)
                        gr.DrawCurve(penPulse, pulsePoints, 1.0F);
                    scroll = (int)(Math.Ceiling(_pulseData.Length * scale)) + axisWidth;
                }
            }
        }

        // простой график всей волны
        public void DrawPulseWaveDataSimple(Graphics gr, Rectangle rct, out int scroll)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gr.Clear(Color.White);
            if (_pulseData != null || rct.Width <= 0)
            {
                Point[] pulsePoints = new Point[Math.Min(rct.Width, Math.Max(_pulseData.Length - rct.Left, 0))];
                Pen penPulse = Pens.Black;
                using (Font fontSecondsBig = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                    fontSecondsSmall = new Font("Microsoft Sans Serif", 8, FontStyle.Regular))
                {
                    const string strAxis = "сек";
                    const int axisFree = 30;
                    int axisWidth = axisFree + (int)Math.Ceiling(gr.MeasureString(strAxis, fontSecondsBig).Width),
                        axisHeight = (int)fontSecondsBig.GetHeight(gr) + 2;
                    uint allH = (ushort)(rct.Height - axisHeight - 10);
                    uint min = _pulseData.Min(), max = _pulseData.Max();
                    int curX = 0, iIndex = rct.Left;
                    while ((curX < pulsePoints.Length) && (iIndex < _pulseData.Length))
                    {
                        uint curH = (uint)(_pulseData[iIndex] - min);
                        Point curPoint = new Point(curX, (int)(rct.Bottom - axisHeight - 5 - curH * allH / (max - min)));
                        pulsePoints[curX] = curPoint;
                        curX++;
                        iIndex++;
                    }
                    // оси
                    Pen penLines = Pens.DimGray;
                    int axisLevel = rct.Bottom - axisHeight;
                    gr.DrawLine(penLines, 0, axisLevel, curX + axisFree, axisLevel);
                    gr.DrawLine(penLines, curX + axisFree, axisLevel, curX + axisFree - 8, axisLevel - 3);
                    gr.DrawLine(penLines, curX + axisFree, axisLevel, curX + axisFree - 8, axisLevel + 3);
                    gr.DrawString(strAxis, fontSecondsBig, Brushes.Black, curX + axisFree, axisLevel + 2);
                    int axisX = -rct.Left,
                        axisBigSeparator = (int)Math.Round(_pulseData.Length / 20d * 1),
                        axisLastText = axisX;
                    while (axisX <= curX)
                    {
                        double val = ((rct.Left + axisX) / 1d) * 20 / _pulseData.Length;
                        gr.DrawLine(penLines, axisX, axisLevel, axisX, axisLevel + 7);
                        string axisText = val.ToString("0");
                        gr.DrawString(axisText, fontSecondsBig, Brushes.Black, axisX, axisLevel + 2);
                        axisLastText = axisX + (int)gr.MeasureString(axisText, fontSecondsBig).Width * 2;
                        for (int i = 1; i < 10; i++)
                        {
                            int tempAxisX = axisX + (int)Math.Round(axisBigSeparator / 10f * i);
                            if (tempAxisX > curX)
                                break;
                            gr.DrawLine(penLines, tempAxisX, axisLevel, tempAxisX, axisLevel + 2);
                            if (tempAxisX > axisLastText)
                            {
                                val = ((rct.Left + tempAxisX) / 1d) * 20 / _pulseData.Length;
                                axisText = val.ToString("0.#", CultureInfo.CurrentCulture);
                                if (tempAxisX + gr.MeasureString(axisText, fontSecondsSmall).Width < axisX + axisBigSeparator)
                                {
                                    gr.DrawString(axisText, fontSecondsSmall,
                                                  Brushes.Gray, tempAxisX, axisLevel + 3);
                                    axisLastText = tempAxisX + (int)gr.MeasureString(axisText, fontSecondsSmall).Width * 2;
                                }
                            }
                        }
                        axisX += axisBigSeparator;
                    }
                    // рисуем кривую
                    if (pulsePoints.Length > 1)
                        gr.DrawCurve(penPulse, pulsePoints, 1.0F);
                    scroll = _pulseData.Length + axisWidth;
                }
            }
            else
                scroll = 0;
        }

        private double _scale = 1.0;
        private int _reperWidth, _padding;
        private bool _flagDrawPressAxis = false;
        private const int PressAxisWidth = 40;

        private struct ReperLine
        {
            public Pen ReperPen { get; set; }
            public Point Point1 { get; set; }
            public Point Point2 { get; set; }
        }

        // прорисовка средней ПВ
        public void DrawPulseWave(Graphics gr, Rectangle rct, Point p, out int scroll, bool razmetka, bool center = false,
            double scale = 1, bool points = false, bool printVals = false, bool printMode = false, bool flagDrawPressAxis = false, bool flagDrawGrid = false)
        {
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (!center)
                gr.FillRectangle(new SolidBrush(Color.White), new Rectangle(p, new Size(rct.Width, rct.Height)));
            if (PulseWaveField != null)
            {
                _scale = scale;
                _flagDrawPressAxis = flagDrawPressAxis;
                Point[] pulsePoints = new Point[(int)Math.Min(Math.Round((rct.Width - (_flagDrawPressAxis ? PressAxisWidth : 0)) / scale),
                    Math.Max(Math.Round(PulseWaveField.Length - rct.Left / scale), 0))];
                using (Pen penPulse = new Pen(Color.Red, 2),
                           penTags = new Pen(Color.Gray),
                           penLines = new Pen(Color.DimGray),
                           penGrid = new Pen(Color.LightGray))
                {
                    //var str2 = String.Join(", ", new List<ushort>(PulseWaveField).ConvertAll(i => i.ToString()).ToArray());
                    //var a1 = _repers[0];
                    //var a4 = _repers[3];
                    if (center)
                        penPulse.Color = Color.Black;
                    using (Font fontText = new Font("Microsoft Sans Serif", 8),
                                fontSecondsBig = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                                fontSecondsSmall = new Font("Microsoft Sans Serif", 8, FontStyle.Regular))
                    {
                        const string strAxis = "сек";
                        const int axisFree = 30;
                        int axisWidth = axisFree + (int)Math.Ceiling(gr.MeasureString(strAxis, fontSecondsBig).Width),
                            axisHeight = (!center ? (int)fontSecondsBig.GetHeight(gr) + 2 : 10),
                            textHeight = (razmetka ? (int)gr.MeasureString(RepersLabel + "1", (_flagDrawPressAxis ? fontSecondsBig : fontText)).Height : 10);
                        _reperWidth = (int)gr.MeasureString(RepersLabel + "1", fontText).Width;
                        _padding = textHeight;
                        uint allH = (uint)(rct.Height - textHeight - axisHeight),
                             min = PulseWaveField.Min(),
                             max = (uint)Math.Max(PulseWaveField.Max(), _maxValAll);
                        int iPoint = 0,
                            startX = center ? (int)(rct.Width / 2d - PulseWaveField.Length * scale / 2) : Math.Max((int)Math.Round(StartDelay * scale - rct.Left), 0);
                        // рисуем шкалу давления
                        if (_flagDrawPressAxis)
                        {
                            startX += PressAxisWidth;
                            gr.DrawLine(penLines, p.X + PressAxisWidth, p.Y + rct.Top, p.X + PressAxisWidth, p.Y + rct.Bottom);
                            string axisText = _lower.ToString("0");
                            SizeF textSize = gr.MeasureString(axisText, fontSecondsBig);
                            gr.DrawString(axisText, fontSecondsBig, Brushes.Black, p.X + PressAxisWidth - textSize.Width, p.Y + rct.Bottom - axisHeight - textSize.Height);

                            int pressNumber = (int)(allH / fontSecondsSmall.Height / 2),
                                curY = rct.Bottom - axisHeight,
                                curYDelta = (int)Math.Round((double)allH / pressNumber);
                            float pressDelta = (float)(_top - _lower) / pressNumber,
                                  pressCur = _lower;
                            for (int i = 0; i < pressNumber - 1; i++)
                            {
                                if (flagDrawGrid && i == 0)
                                    for (int j = 0; j < 3; j++)
                                        gr.DrawLine(penGrid, p.X + PressAxisWidth, p.Y + curY - j * curYDelta / 3f, p.X + rct.Right, p.Y + curY - j * curYDelta / 3f);
                                pressCur += pressDelta;
                                curY -= curYDelta;
                                gr.DrawLine(penLines, p.X + PressAxisWidth, p.Y + curY, p.X + PressAxisWidth - 2, p.Y + curY);
                                if (flagDrawGrid)
                                    for (int j = 0; j < 3; j++)
                                        gr.DrawLine(penGrid, p.X + PressAxisWidth, p.Y + curY - j * curYDelta / 3f, p.X + rct.Right, p.Y + curY - j * curYDelta / 3f);
                                //gr.DrawLine(penLines, p.X, p.Y + axisLevel, p.X + Math.Min((int)curX + axisFree, rct.Right), p.Y + axisLevel);
                                axisText = pressCur.ToString("0");
                                textSize = gr.MeasureString(axisText, fontSecondsSmall);
                                gr.DrawString(axisText, fontSecondsSmall, Brushes.Gray, p.X + PressAxisWidth - textSize.Width, p.Y + curY - textSize.Height);
                            }

                            axisText = _top.ToString("0");
                            textSize = gr.MeasureString(axisText, fontSecondsBig);
                            gr.DrawString(axisText, fontSecondsBig, Brushes.Black, p.X + PressAxisWidth - textSize.Width, p.Y + rct.Top);
                        }
                        // рисуем график
                        double curX = 0;
                        var repersLines = new List<ReperLine>();   // чтобы прочертить реперы еще раз поверх сетки
                        while ((curX < rct.Width) && (iPoint + (int)(rct.Left / scale) < (PulseWaveField.Length + StartDelay)) &&
                               (iPoint >= 0) && (iPoint < pulsePoints.Length))
                        {
                            int iIndex = iPoint + Math.Max((int)(Math.Round(rct.Left / scale)) - StartDelay, 0);
                            uint curH = 0;
                            if (iIndex < PulseWaveField.Length)
                                curH = (uint)(PulseWaveField[iIndex] - min);
                            Point curPoint = new Point(p.X + startX + (int)curX,
                                                       p.Y + (int)(rct.Bottom - axisHeight - curH * allH / (max - min)));
                            pulsePoints[iPoint] = curPoint;
                            // разметка
                            if (razmetka && _repers != null)
                            {
                                for (int i = 0; i < _repers.Length; i++)
                                {
                                    if (_repers[i].Position == iIndex && _repers[i].Visible)
                                    {
                                        Color curColor = penTags.Color;
                                        int curWidth = 1;
                                        Font curFont = fontText;
                                        if (_idMouseMove == i)
                                        {
                                            curColor = Color.Red;
                                            curWidth = 2;
                                            if (MoveReperMode)
                                            {
                                                //curWidth = 3;
                                                curFont = new Font(curFont, FontStyle.Bold);
                                            }
                                        }
                                        Pen curPen = new Pen(curColor, curWidth);
                                        SolidBrush curBrush = new SolidBrush(curColor);
                                        gr.DrawLine(curPen, new Point(curPoint.X, p.Y + textHeight),
                                                    new Point(curPoint.X, p.Y + rct.Bottom - axisHeight));
                                        string strReper = RepersLabel + (i + 1);
                                        gr.DrawString(strReper, curFont, curBrush,
                                                      new PointF(
                                                          curPoint.X - gr.MeasureString(strReper, curFont).Width / 2,
                                                          p.Y));
                                        // запомним
                                        repersLines.Add(new ReperLine()
                                        {
                                            ReperPen = curPen,
                                            Point1 = new Point(curPoint.X, p.Y + textHeight),
                                            Point2 = new Point(curPoint.X, p.Y + rct.Bottom - axisHeight)
                                        });
                                    }
                                }
                            }
                            // значения
                            if (printVals)
                            {
                                using (Font fontTextData = new Font("Microsoft Sans Serif", 6))
                                {
                                    gr.DrawString(PulseWaveField[iIndex].ToString(), fontTextData,
                                                  new SolidBrush(penPulse.Color),
                                                  new Point(curPoint.X, curPoint.Y + 2));
                                }
                            }
                            curX += scale;
                            iPoint++;
                        }
                        if (razmetka)
                        {
                            gr.DrawLine(penTags, new Point(p.X, p.Y + textHeight),
                                        new Point(p.X + rct.Width, p.Y + textHeight));
                        }
                        // точки
                        if (points)
                        {
                            foreach (var pulsePoint in pulsePoints)
                                gr.FillEllipse(new SolidBrush(penPulse.Color), pulsePoint.X - 3, pulsePoint.Y - 3, 6, 6);
                        }
                        // оси
                        if (!center)
                        {
                            if (!printMode)
                            {
                                double prirostDoCelogo = (1 - (((rct.Left + curX) / scale) * 20d / _pulseData.Length) % 1) *
                                                         _pulseData.Length * scale / 20d;
                                curX += Math.Ceiling(prirostDoCelogo);
                                axisWidth += (int)Math.Ceiling(prirostDoCelogo);
                            }
                            int axisLevel = rct.Bottom - axisHeight;
                            float axisX = -rct.Left + (_flagDrawPressAxis ? PressAxisWidth : 0);
                            float axisBigSeparator = (float)(_pulseData.Length / 20d * scale);
                            float axisXDevSep = axisBigSeparator / 10f;
                            while (axisX <= curX + axisBigSeparator / 20f)
                            {
                                double val = ((rct.Left + axisX - (_flagDrawPressAxis ? PressAxisWidth : 0)) / scale) * 20 / _pulseData.Length;
                                gr.DrawLine(penLines, p.X + axisX, p.Y + axisLevel, p.X + axisX, p.Y + axisLevel + 7);
                                if (flagDrawGrid)
                                    gr.DrawLine(penGrid, p.X + axisX, p.Y + textHeight, p.X + axisX, p.Y + rct.Bottom - axisHeight);
                                string axisText = val.ToString("0");
                                gr.DrawString(axisText, fontSecondsBig, Brushes.Black, p.X + axisX, p.Y + axisLevel + 2);
                                var axisLastText = axisX + gr.MeasureString(axisText, fontSecondsBig).Width * 2;
                                for (int i = 1; i < 10; i++)
                                {
                                    var tempAxisX = axisX + axisXDevSep * i;
                                    if (tempAxisX > curX)
                                        break;
                                    gr.DrawLine(penLines, p.X + tempAxisX, p.Y + axisLevel, p.X + tempAxisX, p.Y + axisLevel + 2);
                                    if (flagDrawGrid)
                                        for (int j = 0; j < 4; j++)
                                            gr.DrawLine(penGrid, p.X + tempAxisX - j * axisXDevSep / 4f, p.Y + textHeight, p.X + tempAxisX - j * axisXDevSep / 4f, p.Y + rct.Bottom - axisHeight);
                                    if (tempAxisX > axisLastText)
                                    {
                                        val = ((rct.Left + tempAxisX - (_flagDrawPressAxis ? PressAxisWidth : 0)) / scale) * 20 / _pulseData.Length;
                                        if (scale >= 3)
                                            axisText = val.ToString("0.##", CultureInfo.CurrentCulture);
                                        else
                                            axisText = val.ToString("0.#", CultureInfo.CurrentCulture);
                                        if (tempAxisX + gr.MeasureString(axisText, fontSecondsSmall).Width <
                                            axisX + axisBigSeparator)
                                        {
                                            gr.DrawString(axisText, fontSecondsSmall,
                                                          Brushes.Gray, p.X + tempAxisX, p.Y + axisLevel + 3);
                                            axisLastText = tempAxisX +
                                                           (int)gr.MeasureString(axisText, fontSecondsSmall).Width * 2;
                                        }
                                    }
                                }
                                axisX += axisBigSeparator;
                            }
                            gr.DrawLine(penLines, p.X, p.Y + axisLevel, p.X + Math.Min((int)curX + axisFree, rct.Right), p.Y + axisLevel);
                            if ((int)curX + axisFree < rct.Right)
                            {
                                gr.DrawLine(penLines, p.X + (int)curX + axisFree, p.Y + axisLevel,
                                            p.X + (int)curX + axisFree - 8,
                                            p.Y + axisLevel - 3);
                                gr.DrawLine(penLines, p.X + (int)curX + axisFree, p.Y + axisLevel,
                                            p.X + (int)curX + axisFree - 8,
                                            p.Y + axisLevel + 3);
                                gr.DrawString(strAxis, fontSecondsBig, Brushes.Black, p.X + (int)curX + axisFree,
                                              p.Y + axisLevel + 2);
                            }
                            if (_flagDrawPressAxis)
                                gr.DrawLine(penLines, p.X + PressAxisWidth, p.Y + rct.Top, p.X + PressAxisWidth, p.Y + rct.Bottom);
                        }
                        // разметка (еще раз прочертим реперы
                        repersLines.ForEach(delegate (ReperLine reper)
                        {
                            gr.DrawLine(reper.ReperPen, reper.Point1, reper.Point2);
                        });
                        scroll = (int)(Math.Ceiling((PulseWaveField.Length + StartDelay) * scale)) + axisWidth;
                    }
                    // кривая
                    if (pulsePoints.Length > 1)
                        gr.DrawCurve(penPulse, pulsePoints, 1.0F);
                }
            }
            else
                scroll = 0;
        }

        // --------------- стресс -------------------

        public int GetStressMinLength()
        {
            int minLength = 0;
            if (_arrBegin.Count >= 3)
                minLength = (int)_arrBegin[2] - (int)_arrBegin[0];
            for (int i = 1; i < _arrBegin.Count - 2; i++)
                if (((int)_arrBegin[i + 2] - (int)_arrBegin[i]) < minLength)
                    minLength = (int)_arrBegin[i + 2] - (int)_arrBegin[i];
            return minLength;
        }

        public int GetStressMaxLength()
        {
            int maxLength = 0;
            for (int i = 0; i < _arrBegin.Count - 2; i++)
                if (((int)_arrBegin[i + 2] - (int)_arrBegin[i]) > maxLength)
                    maxLength = (int)_arrBegin[i + 2] - (int)_arrBegin[i];
            return maxLength;
        }

        public Point[][] GetPulseStressPoints(Rectangle rct, Point p, out int scroll, bool center = false, double scale = 1)
        {
            if (_pulseData != null)
            {
                Point[][] pulsePoints = new Point[_arrBegin.Count - 2][];
                int startX = 0,
                    maxLength = GetStressMaxLength();
                if (center)
                    startX = Math.Max((int)(rct.Width / 2.0 - maxLength * scale / 2), 0);
                int sredChina = 0;
                for (int i = 0; i < _arrChina.Count - 1; i++)
                    sredChina += _pulseData[(int)_arrChina[i]];
                sredChina /= (_arrChina.Count - 1);
                uint min = _pulseData[(int)_arrBegin[0]], max = _pulseData[(int)_arrBegin[0]];
                for (int i = (int)_arrBegin[0]; i < (int)_arrBegin[_arrBegin.Count - 1]; i++)
                {
                    if (_pulseData[i] < min)
                        min = _pulseData[i];
                    if (_pulseData[i] > max)
                        max = _pulseData[i];
                }
                uint h = (uint)(sredChina - min), H = (ushort)(rct.Height - (center ? 40 : 20));
                int levelChina = (int)(rct.Bottom - (center ? 20 : 10) - h * H / (max - min));

                for (int i = 0; i < _arrBegin.Count - 2; i++)
                {
                    int iPoint = 0;
                    int curLength = (int)_arrBegin[i + 2] - (int)_arrBegin[i];
                    pulsePoints[i] = new Point[(int)Math.Min(Math.Round(rct.Width / scale),
                        Math.Max(Math.Round(curLength - rct.Left / scale), 0))];
                    double curX = 0;
                    while ((curX < rct.Width) && (iPoint + (int)(rct.Left / scale) < curLength) &&
                        (iPoint >= 0) && (iPoint < pulsePoints[i].Length))
                    {
                        int iIndex = (int)_arrBegin[i] + iPoint + (int)(Math.Round(rct.Left / scale));
                        Point curPoint = new Point(p.X + startX + (int)curX,
                                                   p.Y + levelChina +
                                                   (int)
                                                   ((_pulseData[(int)_arrChina[i]] - _pulseData[iIndex]) * H / (max - min)));
                        if (curPoint.Y < p.Y)
                            curPoint.Y = p.Y - 1;
                        if (curPoint.Y > p.Y + rct.Height)
                            curPoint.Y = p.Y + rct.Height + 1;
                        pulsePoints[i][iPoint] = curPoint;
                        curX += scale;
                        iPoint++;
                    }
                }
                scroll = (int)(Math.Ceiling(maxLength * scale));
                return pulsePoints;
            }
            else
            {
                scroll = 0;
                return null;
            }
        }

        public void DrawPulseStress(Graphics gr, Rectangle rct, Point p, out int scroll, bool center = false, double scale = 1)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (!center)
                gr.FillRectangle(new SolidBrush(Color.White), new Rectangle(p, new Size(rct.Width, rct.Height)));
            Point[][] stressPoints = GetPulseStressPoints(rct, p, out scroll, center, scale);
            if (stressPoints != null)
            {
                Pen penPulse = Pens.Black;
                for (int i = 0; i < stressPoints.Length; i++)
                {
                    if (stressPoints[i].Length > 1)
                        gr.DrawLines(penPulse, stressPoints[i]);
                }
            }
        }


        // --------------- возраст -------------------

        public int CalcAge()
        {
            if (PulseWaveField == null || _repers == null || _repers.Length < 4 ||
                _repers[2].Position >= PulseWaveField.Length || _repers[3].Position >= PulseWaveField.Length)
                return 0;
            float val = (float)PulseWaveField[_repers[2].Position] / PulseWaveField[_repers[3].Position];
            if (val < 0.5)
            {
                int res = CalcAge(15, 25, 0, 0.5f, val);
                return (res >= 20 ? res : 20);
            }
            else if (val >= 0.5 && val <= 0.8)
                return CalcAge(25, 35, 0.5f, 0.8f, val);
            else if (val > 0.8 && val <= 0.91)
                return CalcAge(35, 45, 0.8f, 0.91f, val);
            else if (val > 0.91 && val <= 1.1)
                return CalcAge(45, 55, 0.91f, 1.1f, val);
            else if (val > 1.1)
            {
                int res = CalcAge(55, 80, 1.1f, 1.7f, val);
                return (res <= 80 ? res : 80);
            }
            return 50;
        }

        private static int CalcAge(int age1, int age2, float p1, float p2, float p)
        {
            return (int)Math.Round(Math.Abs(p - p1) * (age2 - age1) / (p2 - p1)) + age1;
        }


        // --------------- кровоток -------------------

        public int CalcBlood(float k1, out Double val)
        {
            if (PulseWaveField == null || _repers == null || _repers.Length < 2 ||
                _repers[0].Position >= PulseWaveField.Length || _repers[1].Position >= PulseWaveField.Length)
            {
                val = 0;
                return 0;
            }
            val = k1 * Math.Pow((Double)PulseWaveField[_repers[1].Position] / PulseWaveField[_repers[0].Position], -0.3) *
                  Math.Pow(Statics.fss / 80d, 2);
            int koef = (int)Math.Round(75 * val);
            if (koef < 60)
                return 60;
            if (koef > 90)
                return 90;
            return koef;
        }

        public int CalcBlood(float k1)
        {
            Double tempBlood;
            return CalcBlood(k1, out tempBlood);
        }


        //*****************************************************************
        //                      Интегралогистограммы                      *
        //*****************************************************************

        private uint[] _pulseIntegrals;

        public void CalculateIntegrals()
        {
            _pulseIntegrals = new uint[_arrBegin.Count - 1];
            for (int i = 0; i < _arrBegin.Count - 1; i++)
            {
                _pulseIntegrals[i] = 0;
                ushort minPulseData = ushort.MaxValue;
                for (int j = (int)_arrBegin[i]; j < (int)_arrBegin[i + 1]; j++)
                {
                    if (_pulseData[j] < minPulseData)
                        minPulseData = _pulseData[j];
                }
                for (int j = (int)_arrBegin[i]; j < (int)_arrBegin[i + 1]; j++)
                {
                    _pulseIntegrals[i] += (uint)(_pulseData[j] - minPulseData);
                }
            }
        }

        public void DrawPulseIntegrals(Graphics gr, Rectangle rct, Point p, out int scroll)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gr.FillRectangle(new SolidBrush(Color.White), new Rectangle(p, new Size(rct.Width, rct.Height)));
            if (_pulseIntegrals != null)
            {
                Pen penIntegral = Pens.Black,
                    penLines = Pens.DimGray;
                Brush brushText = Brushes.Black;
                using (Font fontText = new Font("Microsoft Sans Serif", 10, FontStyle.Bold))
                {
                    // рисуем интегралы
                    const string strAxis = "№";
                    const int axisFree = 30;
                    int axisWidth = axisFree + (int)Math.Ceiling(gr.MeasureString(strAxis, fontText).Width),
                        axisHeight = (int)fontText.GetHeight(gr) + 2,
                        integralWidth = Math.Max((rct.Width - axisWidth) / _pulseIntegrals.Length, 3);
                    ushort allH = (ushort)(rct.Height - axisHeight - 10);
                    uint min = 0, max = _pulseIntegrals.Max();
                    int iIndex = rct.Left / integralWidth, curX = -rct.Left % integralWidth;
                    while ((curX < rct.Width) && (iIndex < _pulseIntegrals.Length))
                    {
                        uint curH = _pulseIntegrals[iIndex] - min;
                        // левая верхняя точка столбика
                        Point curPoint = new Point(curX,
                                                   (int)(rct.Bottom - axisHeight - curH * allH / (max - min)));
                        // метки
                        if ((iIndex + 1) % 5 == 0)
                        {
                            gr.DrawLine(penLines, p.X + curX + integralWidth / 2, p.Y + rct.Bottom - axisHeight - 0,
                                        p.X + curX + integralWidth / 2, p.Y + rct.Bottom - axisHeight + 2);
                            string strAxisVal = (iIndex + 1).ToString();
                            gr.DrawString(strAxisVal, fontText, brushText,
                                          p.X + curX + integralWidth / 2 - gr.MeasureString(strAxisVal, fontText).Width / 2,
                                          p.Y + rct.Bottom - fontText.GetHeight(gr));
                        }
                        // столбик
                        //gr.FillRectangle(brushIntegral, p.X + curPoint.X + 1, p.Y + curPoint.Y,
                        //                 integralWidth - 2, rct.Bottom - axisHeight - 1 - curPoint.Y);
                        var pointsIntegral = new Point[]
                                                 {
                                                     new Point(p.X + curPoint.X, p.Y + rct.Bottom - axisHeight - 1),
                                                     new Point(p.X + curPoint.X, p.Y + curPoint.Y),
                                                     new Point(p.X + curPoint.X + integralWidth, p.Y + curPoint.Y),
                                                     new Point(p.X + curPoint.X + integralWidth,
                                                               p.Y + rct.Bottom - axisHeight - 1)
                                                 };
                        gr.DrawLines(penIntegral, pointsIntegral);
                        curX += integralWidth;
                        iIndex++;
                    }
                    // рисуем ось
                    int axisLevel = rct.Bottom - axisHeight;
                    gr.DrawLine(penLines, p.X, p.Y + axisLevel, p.X + curX + axisFree, p.Y + axisLevel);
                    gr.DrawLine(penLines, p.X + curX + axisFree, p.Y + axisLevel, p.X + curX + axisFree - 8, p.Y + axisLevel - 3);
                    gr.DrawLine(penLines, p.X + curX + axisFree, p.Y + axisLevel, p.X + curX + axisFree - 8, p.Y + axisLevel + 3);
                    gr.DrawString(strAxis, fontText, Brushes.Black, p.X + curX + axisFree, p.Y + axisLevel + 2);
                    scroll = _pulseIntegrals.Length * integralWidth + axisWidth;
                }
            }
            else
                scroll = 0;
        }

        public void DrawPulseIntegralsPhas(Graphics gr, Rectangle rct, Point p, out int scroll)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gr.FillRectangle(new SolidBrush(Color.White), new Rectangle(p, new Size(rct.Width, rct.Height)));
            if (_pulseIntegrals != null)
            {
                Pen penLines = Pens.DimGray;
                using (Font fontWaves = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                            fontSecondsBig = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                            fontSecondsSmall = new Font("Microsoft Sans Serif", 8, FontStyle.Regular))
                {
                    const string strAxis = "сек";
                    const int axisFree = 30;
                    int axisWidth = axisFree + (int)Math.Ceiling(gr.MeasureString(strAxis, fontSecondsBig).Width),
                        axisHeight = (int)fontSecondsBig.GetHeight(gr) + 2;
                    Brush brushIntegral = Brushes.Black;
                    // рисуем интегралы
                    const int integralWidth = 2;
                    ushort allH = (ushort)(rct.Height - axisHeight - 10);
                    // масштаб по горизонтали
                    double scale =
                        Math.Max(
                            ((double)(rct.Width - axisWidth) / ((int)_arrBegin[_arrBegin.Count - 1] - (int)_arrBegin[0])),
                            0.01);
                    uint min = 0, max = _pulseIntegrals.Max();
                    int iIndex = 0;
                    while (iIndex < _arrBegin.Count - 1)
                    {
                        if (scale * ((int)_arrBegin[iIndex] - (int)_arrBegin[0]) >= rct.Left - integralWidth)
                            break;
                        iIndex++;
                    }
                    int curX = -(int)Math.Round(rct.Left - scale * ((int)_arrBegin[iIndex] - (int)_arrBegin[0]));
                    var pointsIntegralList = new ArrayList();
                    while ((curX < rct.Width) && (iIndex < _pulseIntegrals.Length))
                    {
                        uint curH = _pulseIntegrals[iIndex] - min;
                        // левая верхняя точка столбика
                        Point curPoint = new Point(curX,
                                                   (int)(rct.Bottom - axisHeight - curH * allH / (max - min)));
                        pointsIntegralList.Add(new Point(p.X + curPoint.X + integralWidth / 2, p.Y + curPoint.Y));
                        // столбик
                        gr.FillRectangle(brushIntegral, p.X + curPoint.X, p.Y + curPoint.Y,
                                         integralWidth, rct.Bottom - axisHeight - 1 - curPoint.Y);
                        curX += (int)Math.Round(scale * ((int)_arrBegin[iIndex + 1] - (int)_arrBegin[iIndex]));
                        iIndex++;
                    }
                    // оси
                    int axisLevel = rct.Bottom - axisHeight;
                    gr.DrawLine(penLines, p.X, p.Y + axisLevel, p.X + curX + axisFree, p.Y + axisLevel);
                    gr.DrawLine(penLines, p.X + curX + axisFree, p.Y + axisLevel, p.X + curX + axisFree - 8, p.Y + axisLevel - 3);
                    gr.DrawLine(penLines, p.X + curX + axisFree, p.Y + axisLevel, p.X + curX + axisFree - 8, p.Y + axisLevel + 3);
                    gr.DrawString(strAxis, fontSecondsBig, Brushes.Black, p.X + curX + axisFree, p.Y + axisLevel + 2);
                    int axisX = -rct.Left,
                        axisBigSeparator = (int)Math.Round(_pulseData.Length / 20d * scale),
                        axisLastText = axisX;
                    while (axisX <= curX)
                    {
                        double val = ((rct.Left + axisX) / scale) * 20 / _pulseData.Length;
                        gr.DrawLine(penLines, p.X + axisX, p.Y + axisLevel, p.X + axisX, p.Y + axisLevel + 7);
                        string axisText = val.ToString("0");
                        gr.DrawString(axisText, fontSecondsBig, Brushes.Black, p.X + axisX, p.Y + axisLevel + 2);
                        axisLastText = axisX + (int)gr.MeasureString(axisText, fontSecondsBig).Width * 2;
                        for (int i = 1; i < 10; i++)
                        {
                            int tempAxisX = axisX + (int)Math.Round(axisBigSeparator / 10f * i);
                            if (tempAxisX > curX)
                                break;
                            gr.DrawLine(penLines, p.X + tempAxisX, p.Y + axisLevel, p.X + tempAxisX, p.Y + axisLevel + 2);
                            if (tempAxisX > axisLastText)
                            {
                                val = ((rct.Left + tempAxisX) / scale) * 20 / _pulseData.Length;
                                axisText = val.ToString("0.#", CultureInfo.CurrentCulture);
                                if (tempAxisX + gr.MeasureString(axisText, fontSecondsSmall).Width <
                                    axisX + axisBigSeparator)
                                {
                                    gr.DrawString(axisText, fontSecondsSmall,
                                                  Brushes.Gray, p.X + tempAxisX, p.Y + axisLevel + 3);
                                    axisLastText = tempAxisX +
                                                   (int)gr.MeasureString(axisText, fontSecondsSmall).Width * 2;
                                }
                            }
                        }
                        axisX += axisBigSeparator;
                    }
                    // огибающая
                    gr.DrawCurve(new Pen(brushIntegral), (Point[])pointsIntegralList.ToArray(typeof(Point)), 0.7f);
                    scroll = (int)(Math.Ceiling(((int)_arrBegin[_arrBegin.Count - 1] - (int)_arrBegin[0]) * scale)) +
                             axisWidth;
                }
            }
            else
                scroll = 0;
        }

        public void DrawPulseIntegralsPhasOld(Graphics gr, Rectangle rct, Point p, out int scroll)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gr.FillRectangle(new SolidBrush(Color.White), new Rectangle(p, new Size(rct.Width, rct.Height)));
            if (_pulseIntegrals != null)
            {
                Pen penLines = new Pen(Color.DimGray);
                Font fontText = new Font("Microsoft Sans Serif", 8);
                SolidBrush brushIntegral = new SolidBrush(Color.Black),
                    brushText = new SolidBrush(penLines.Color);
                int fontHeight = (int)fontText.GetHeight(gr) + 2;
                // рисуем интегралы
                string strAxis = "(с)";
                int axisWidth = 20 + (int)Math.Ceiling(gr.MeasureString(strAxis, fontText).Width);
                const int integralWidth = 2;
                ushort allHeight = (ushort)(rct.Height - fontHeight - 10);
                // масштаб по горизонтали
                double scale =
                    Math.Max(
                        ((double)(rct.Width - axisWidth) / ((int)_arrBegin[_arrBegin.Count - 1] - (int)_arrBegin[0])),
                        0.01);
                uint min = 0, max = _pulseIntegrals.Max();
                int iIndex = 0;
                while (iIndex < _arrBegin.Count - 1)
                {
                    if (scale * ((int)_arrBegin[iIndex] - (int)_arrBegin[0]) >= rct.Left - integralWidth)
                        break;
                    iIndex++;
                }
                int curX = -(int)Math.Round(rct.Left - scale * ((int)_arrBegin[iIndex] - (int)_arrBegin[0]));
                var pointsIntegralList = new ArrayList();
                while ((curX < rct.Width) && (iIndex < _pulseIntegrals.Length))
                {
                    uint curHeight = _pulseIntegrals[iIndex] - min;
                    // левая верхняя точка столбика
                    Point curPoint = new Point(curX, (int)(rct.Bottom - fontHeight - curHeight * allHeight / (max - min)));
                    pointsIntegralList.Add(new Point(p.X + curPoint.X + integralWidth / 2, p.Y + curPoint.Y));
                    // столбик
                    gr.FillRectangle(brushIntegral, p.X + curPoint.X, p.Y + curPoint.Y,
                                     integralWidth, rct.Bottom - fontHeight - 1 - curPoint.Y);
                    curX += (int)Math.Round(scale * ((int)_arrBegin[iIndex + 1] - (int)_arrBegin[iIndex]));
                    iIndex++;
                }
                // огибающая
                gr.DrawCurve(new Pen(brushIntegral), (Point[])pointsIntegralList.ToArray(typeof(Point)), 0.7f);
                // рисуем линии
                gr.DrawLine(penLines, p.X + 0, p.Y + rct.Bottom - fontHeight, p.X + curX + 20, p.Y + rct.Bottom - fontHeight);
                gr.DrawLine(penLines, p.X + curX + 20, p.Y + rct.Bottom - fontHeight, p.X + curX + 15, p.Y + rct.Bottom - fontHeight - 3);
                gr.DrawLine(penLines, p.X + curX + 20, p.Y + rct.Bottom - fontHeight, p.X + curX + 15, p.Y + rct.Bottom - fontHeight + 3);
                // пишем единицы измерения
                gr.DrawString(strAxis, fontText, brushText,
                              p.X + curX + 20,
                              p.Y + rct.Bottom - fontText.GetHeight(gr));
                // метки
                int xAxisVal = (int)(-rct.Left % (Frequency * scale) + Frequency * scale),
                    axisVal = rct.Left / (int)(Frequency * scale) + 1;
                while (xAxisVal <= curX + 10)
                {
                    gr.DrawLine(penLines, p.X + xAxisVal, p.Y + rct.Bottom - fontHeight - 2,
                                p.X + xAxisVal, p.Y + rct.Bottom - fontHeight + 2);
                    string strAxisVal = axisVal.ToString();
                    gr.DrawString(strAxisVal, fontText, brushText,
                                  p.X + xAxisVal - gr.MeasureString(strAxisVal, fontText).Width / 2,
                                  p.Y + rct.Bottom - fontText.GetHeight(gr));
                    xAxisVal += (int)Math.Round(scale * Frequency);
                    axisVal++;
                }
                scroll = (int)(Math.Ceiling(((int)_arrBegin[_arrBegin.Count - 1] - (int)_arrBegin[0]) * scale)) + axisWidth;
            }
            else
                scroll = 0;
        }


        //*****************************************************************
        //                  Перемещение реперов мышью                     *
        //*****************************************************************

        private int _idMouseMove = -1;

        public bool MouseMove(Point loc, Rectangle rct, out bool refresh)
        {
            refresh = false;
            if (_repers == null)
                return false;
            loc.X -= (_flagDrawPressAxis ? PressAxisWidth : 0);
            for (int i = 0; i < _repers.Length; i++)
            {
                if (!_repers[i].Visible)
                    continue;
                if (PointInReper(i, loc, rct))
                {
                    if (i == 3)
                        System.Threading.Thread.Sleep(10);
                    int idMouseMoveNew = i;
                    if (_idMouseMove != idMouseMoveNew)
                    {
                        refresh = true;
                        _idMouseMove = idMouseMoveNew;
                    }
                    return true;
                }
            }
            if (_idMouseMove != -1)
                refresh = true;
            _idMouseMove = -1;
            return false;
        }

        // проверка, что курсор над репером
        private bool PointInReper(int i, Point loc, Rectangle rct)
        {
            if (_repers == null || i >= _repers.Length)
                return false;
            int curX = (int)(Math.Max(-rct.Left, 0) - rct.Left + (_repers[i].Position + StartDelay) * _scale);
            if (loc.X >= curX - _reperWidth / 2 && loc.X <= curX + _reperWidth / 2 && loc.Y < rct.Bottom - _padding)
                return true;
            return false;
        }

        public bool MoveReperMode = false;

        public bool MouseDown(Point loc)
        {
            if (_repers == null)
                return false;
            if (_idMouseMove >= 0 && _idMouseMove < _repers.Length)
            {
                MoveReperMode = true;
                return true;
            }
            return false;
        }

        public bool MouseUp()
        {
            if (MoveReperMode)
            {
                MoveReperMode = false;
                _flagRepersFixed = true;
                return true;
            }
            return false;
        }

        public bool ReperMove(Point loc, Rectangle rct)
        {
            if (_repers == null)
                return false;
            if (_idMouseMove >= 0 && _idMouseMove < _repers.Length)
            {
                int newX = (int)Math.Round((loc.X - Math.Max(-rct.Left, 0) + rct.Left - (_flagDrawPressAxis ? PressAxisWidth : 0)) / _scale) - StartDelay;
                if (newX < 0)
                    newX = 0;
                if (newX > PulseWaveField.Length - 1)
                    newX = PulseWaveField.Length - 1;
                _repers[_idMouseMove].Position = newX;
                return true;
            }
            return false;
        }

    }
}
